# Building Biosim4
## Prerequisites
- the source code `git clone https://github.com/davidrmiller/biosim4.git`
- make
- A C++ compiler - trying with gcc 11.2.0 and g++ 11.2.0 (installed separately to gcc on Ubuntu)
- cimg-dev (on Ubuntu 21, installs OpenCV as a dependency)

## Build

Run `make` in the top directory and it builds everything.

## On macOS

On macOS, the Xcode C++ compiler fails to build the source code which might be in an unsuppoerted version of C++.