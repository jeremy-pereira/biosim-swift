// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "biosim-swift",
	platforms: [
		.macOS(.v12),
	],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
		.library(name: "EvoKit", targets: ["EvoKit"]),
        .library(name: "biosim-swift", targets: ["biosim-swift"]),
		.executable(name: "biosim", targets: ["biosim"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
		.package(url: "https://github.com/apple/swift-argument-parser", from: "1.0.2"),
		.package(name: "Toolbox", url: "https://jeremy-pereira@bitbucket.org/jeremy-pereira/Toolbox.git", from: "21.1.0"),
   ],
    targets: [
		.target(name: "cimg_shim"),
        .target(
            name: "biosim-swift",
            dependencies: ["Toolbox", "EvoKit", "cimg_shim"]),
		.executableTarget(
			name: "biosim",
			dependencies: [
				"biosim-swift",
				.product(name: "ArgumentParser", package: "swift-argument-parser")
			]),
		.target(
			name: "EvoKit",
			dependencies: ["Toolbox"]),
        .testTarget(
            name: "biosim-swiftTests",
            dependencies: ["biosim-swift"]),
		.testTarget(
			name: "EvoKitTests",
			dependencies: ["EvoKit"]),
    ]
)
