//
//  SimulationTests.swift
//  
//
//  Created by Jeremy Pereira on 19/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import biosim_swift
import XCTest

final class SimulationTests: XCTestCase
{

	/// Test that the default set of enabled actions includes all of them except
	/// `.killForward` and that the first action is to set responsiveness.
	func testInitEnabledActions()
	{
		do
		{
			let simulation = try Simulation(parameters: ParamManager())
			let enabledActions = simulation.enabledActions
			guard enabledActions.count > 0
			else
			{
				XCTFail("No actions are enabled")
				return
			}
			XCTAssert(enabledActions.count == Action.allCases.count - 1)
			XCTAssert(enabledActions[0] == .setResponsiveness)
			XCTAssert(!enabledActions.contains(.killForward))
		}
		catch
		{
			XCTFail("\(error)")
		}
	}
}
