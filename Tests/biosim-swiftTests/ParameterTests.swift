//
//  ParameterTests.swift
//  
//
//  Created by Jeremy Pereira on 04/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import biosim_swift
import XCTest

final class ParameterTests: XCTestCase
{
	func testDefaultValues()
	{
		let p: ParamManager
		do
		{
			p = try ParamManager()
		}
		catch
		{
			XCTFail("\(error)")
			return
		}
		XCTAssert(try p.sizeX() == 128)
		XCTAssert(try p.sizeY() == 128)
		XCTAssert(try p.signalLayers() == 1)
		XCTAssert(try p.population() == 100)
		XCTAssert(try p.replaceBarrierTypeGenerationNumber() == -1)
		XCTAssert(try p.replaceBarrierType() == Grid.BarrierType.none)
		XCTAssert(try p.barrierType() == Grid.BarrierType.none)
		XCTAssert(try p.genomeInitialLengthMin() == 24)
		XCTAssert(try p.genomeInitialLengthMax() == 24)
		XCTAssert(try p.longProbeDistance() == 24)
		XCTAssert(try p.maxNumberNeurons() == 5)
		XCTAssert(try p.maxGenerations() == 200_000)
		XCTAssert(try p.stepsPerGeneration() == 300)
		XCTAssert(try p.videoStride() == 25)
		XCTAssert(try p.genomeAnalysisStride() == 25)
		XCTAssert(try p.displaySampleGenomes() == 5)
		XCTAssert(try p.populationSensorRadius() == 2.5)
		XCTAssert(try p.signalSensorRadius() == 2.0)
		XCTAssert(try p.shortProbeBarrierDistance() == 4)
		XCTAssert(try p.enableKillForward() == false)
		XCTAssert(try p.responsivenessCurveFactor() == 2.0)
		XCTAssert(try p.challenge() == "cornerWeighted")
		XCTAssert(try p.chooseParentsByFitness() == true)
		XCTAssert(try p.sexualReproduction() == true)
		XCTAssert(try p.pointMutationRate() == 0.00025)
		XCTAssert(try p.saveVideo() == true)
		XCTAssert(try p.videoSaveFirstFrames() == 2)
		XCTAssert(try p.videoStride() == 25)
		XCTAssert(try p.displayScale() == 8)
		XCTAssert(try p.imageDir() == "images")
		XCTAssert(try p.agentSize() == 4)
	}
}
