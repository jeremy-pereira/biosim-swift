//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import biosim_swift

final class BasicTypesTests: XCTestCase
{

	/// Basic tests from biosim4
    func testDirBasics() throws
	{
		var d1 = Direction.north
		let d2 = Direction.centre
		XCTAssert(d1 != d2)
		d1 = d2
		XCTAssert(d1 == d2)
    }

	/// `.centre` must be the highest value ein the enum
	func testCentreInRightPlace()
	{
		for direction in Direction.allCases
		{
			XCTAssert(direction.rawValue < Direction.centre.rawValue || direction == .centre,
					  "\(direction) has rawValue higherr than .centre")
		}
	}

	/// Tests the raw values are as per Biosim4
	///
	///  The numbers don't really make sense, but we'll keep them the same to avoid confusion.
	func testDirectionEnumerationValues()
	{
		XCTAssert(Direction.southWest.asInt() == 0)
		XCTAssert(Direction.south.asInt() == 1)
		XCTAssert(Direction.southEast.asInt() == 2)
		XCTAssert(Direction.west.asInt() == 3)
		XCTAssert(Direction.centre.asInt() == 4)
		XCTAssert(Direction.east.asInt() == 5)
		XCTAssert(Direction.northWest.asInt() == 6)
		XCTAssert(Direction.north.asInt() == 7)
		XCTAssert(Direction.northEast.asInt() == 8)
	}

	func testDirectionEquatable()
	{
		let d1 = Direction.north
		let d2 = Direction.north
		XCTAssert(d1 == d2)
		let d3 = Direction.northEast
		XCTAssert(d1 != d3)
	}

	func testDirectionRotation()
	{
		let d1 = Direction.northEast
		XCTAssert(d1.rotated(1) == .east)
		XCTAssert(d1.rotated(2) == .southEast)
		XCTAssert(d1.rotated(-1) == .north)
		XCTAssert(d1.rotated(-2) == .northWest)
	}

	func testDirectionAsVector()
	{
		var v = Direction.centre.normalisedVector
		XCTAssert(v.x == 0 && v.y == 0)
		v = Direction.southWest.normalisedVector
		XCTAssert(v.x == -1 && v.y == -1)
		v = Direction.south.normalisedVector
		XCTAssert(v.x == 0 && v.y == -1)
		v = Direction.southEast.normalisedVector
		XCTAssert(v.x == 1 && v.y == -1)
		v = Direction.west.normalisedVector
		XCTAssert(v.x == -1 && v.y == 0)
		v = Direction.east.normalisedVector
		XCTAssert(v.x == 1 && v.y == 0)
		v = Direction.northWest.normalisedVector
		XCTAssert(v.x == -1 && v.y == 1)
		v = Direction.north.normalisedVector
		XCTAssert(v.x == 0 && v.y == 1)
		v = Direction.northEast.normalisedVector
		XCTAssert(v.x == 1 && v.y == 1)
	}

	func testNormalisedPolar()
	{
		var p = Direction.south.normalisedPolar
		XCTAssert(p.magnitude == 1 && p.direction == Direction.south)
		p = Direction.southEast.normalisedPolar
		XCTAssert(p.magnitude == 1 && p.direction == Direction.southEast)
		p = Direction.west.normalisedPolar
		XCTAssert(p.magnitude == 1 && p.direction == Direction.west)
		p = Direction.east.normalisedPolar
		XCTAssert(p.magnitude == 1 && p.direction == Direction.east)
		p = Direction.northWest.normalisedPolar
		XCTAssert(p.magnitude == 1 && p.direction == Direction.northWest)
		p = Direction.north.normalisedPolar
		XCTAssert(p.magnitude == 1 && p.direction == Direction.north)
		p = Direction.northEast.normalisedPolar
		XCTAssert(p.magnitude == 1 && p.direction == Direction.northEast)
		p = Direction.centre.normalisedPolar
		XCTAssert(p.magnitude == 0 && p.direction == Direction.centre)
	}

	func testIsNormalised()
	{
		XCTAssert(!Vector(9, 101).isNormalised)
		XCTAssert(Vector(0, 0).isNormalised)
		XCTAssert(Vector(0, 1).isNormalised)
		XCTAssert(Vector(1, 1).isNormalised)
		XCTAssert(Vector(-1, 0).isNormalised)
		XCTAssert(Vector(-1, -1).isNormalised)
		XCTAssert(!Vector(0, 2).isNormalised)
		XCTAssert(!Vector(1, 2).isNormalised)
		XCTAssert(!Vector(-1, 2).isNormalised)
		XCTAssert(!Vector(2, 0).isNormalised)
	}

	func testNormaliseAndAsDirection()
	{
		var v1 = Vector(0, 0)
		let v2 = v1.normalised
		XCTAssert(v2.x == 0 && v2.y == 0)
		XCTAssert(v2.direction == .centre)

		v1 = Vector(0, 1).normalised
		XCTAssert(v1.x == 0 && v1.y == 1)
		XCTAssert(v1.direction == .north)

		v1 = Vector(-1, 1).normalised
		XCTAssert(v1.x == -1 && v1.y == 1)
		XCTAssert(v1.direction == .northWest, "Wrong direction: '\(v1.direction)'")

		v1 = Vector(100, 5).normalised
		XCTAssert(v1.x == 1 && v1.y == 0)
		XCTAssert(v1.direction == .east, "Wrong direction: '\(v1.direction)'")

		v1 = Vector(100, 105).normalised
		XCTAssert(v1.x == 1 && v1.y == 1)
		XCTAssert(v1.direction == .northEast, "Wrong direction: '\(v1.direction)'")

		v1 = Vector(-5, 101).normalised
		XCTAssert(v1.x == 0 && v1.y == 1)
		XCTAssert(v1.direction == .north, "Wrong direction: '\(v1.direction)'")

		v1 = Vector(-500, 10).normalised
		XCTAssert(v1.x == -1 && v1.y == 0)
		XCTAssert(v1.direction == .west, "Wrong direction: '\(v1.direction)'")

		v1 = Vector(-500, -490).normalised
		XCTAssert(v1.x == -1 && v1.y == -1)
		XCTAssert(v1.direction == .southWest, "Wrong direction: '\(v1.direction)'")

		v1 = Vector(-1, -490).normalised
		XCTAssert(v1.x == 0 && v1.y == -1)
		XCTAssert(v1.direction == .south, "Wrong direction: '\(v1.direction)'")

		v1 = Vector(1101, -1090).normalised
		XCTAssert(v1.x == 1 && v1.y == -1)
		XCTAssert(v1.direction == .southEast, "Wrong direction: '\(v1.direction)'")

		v1 = Vector(1101, -3).normalised
		XCTAssert(v1.x == 1 && v1.y == 0)
		XCTAssert(v1.direction == .east, "Wrong direction: '\(v1.direction)'")

	}

	func testVectorLength()
	{
		XCTAssert(Vector(0, 0).length == 0)
		XCTAssert(Vector(0, 1).length == 1)
		XCTAssert(Vector(-1, 0).length == 1)
		XCTAssert(Vector(-1, -1).length == 1)
		XCTAssert(Vector(22, 0).length == 22)
		XCTAssert(Vector(22, 22).length == 31)
		XCTAssert(Vector(10, -10).length == 14)
		XCTAssert(Vector(-310, 0).length == 310)
	}
}
