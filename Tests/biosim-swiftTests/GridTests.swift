//
//  GridTests.swift
//  
//
//  Created by Jeremy Pereira on 04/12/2021.
//
//  Copyright (c) Jeremy Pereira @021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


@testable import biosim_swift
import XCTest

final class GridTests: XCTestCase
{
	func testVerticalBarrier()
	{
		var g = Grid(sizeX: 10, sizeY: 8)
		g.createBarrier(type: .verticalBarConstantLocation)
		print(g)
		XCTAssert(g[0, 0] == .empty)
		XCTAssert(g[4, 1] == .empty)
		XCTAssert(g[5, 5] == .barrier)
		XCTAssert(g[4, 2] == .barrier)
		XCTAssert(g[5, 6] == .empty)
		XCTAssert(g[6, 5] == .empty)
	}

	func testRandomBarrier()
	{
		XCTFail("\(Grid.BarrierType.verticalBarRandomLocations) not implemented")
	}

	func testFiveBlocksStaggerd()
	{
		XCTFail("\(Grid.BarrierType.fiveStaggerdVerticalBars) not implemented")
	}

	func testHorizontalBarConstant()
	{
		XCTFail("\(Grid.BarrierType.horizBarConstantLocationNorthCentre) not implemented")
	}

	func testFloatingIslands()
	{
		XCTFail("\(Grid.BarrierType.floatingIslands) not implemented")
	}

	func testSpots()
	{
		XCTFail("\(Grid.BarrierType.sequenceOfSpots) not implemented")
	}

	func testClippedRect()
	{
		let g = Grid(sizeX: 10, sizeY: 10)
		let gridRect = g.rect
		XCTAssert(gridRect == Rect(bottomLeft: Coordinate(0, 0), size: Vector(10, 10)))
		let clippedRect = g.clip(gridRect)
		XCTAssert(clippedRect == gridRect)
	}

	func testReduce0()
	{
		let g = Grid(sizeX: 5, sizeY: 5)
		let centre = Coordinate(2, 2)
		let cellCount = g.reduce(location: centre, radius: 0, initial: 0)
		{
			location, content, partial in
			XCTAssert(location == centre)
			XCTAssert(content == .empty)
			return partial + 1
		}
		XCTAssert(cellCount == 1)
	}

	func testReduce1()
	{
		let g = Grid(sizeX: 5, sizeY: 5)
		let centre = Coordinate(2, 2)
		let cellCount = g.reduce(location: centre, radius: 1, initial: 0)
		{
			location, content, partial in
			XCTAssert(location.x == centre.x || location.y == centre.y, "Erroneous location: \(location)")
			XCTAssert(content == .empty)
			return partial + 1
		}
		XCTAssert(cellCount == 5, "Got cellCount \(cellCount)")
	}

}

