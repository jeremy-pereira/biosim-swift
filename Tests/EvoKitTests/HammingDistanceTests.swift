//
//  HammingDistanceTests.swift
//  
//
//  Created by Jeremy Pereira on 29/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import EvoKit
import XCTest

final class HammingDistanceTests: XCTestCase
{
	func testUInt8And16()
	{
		let a = UInt8.random(in: 0 ... UInt8.max)
		let b = a
		XCTAssert(b.distance(to: a) == 0)
		XCTAssert(b.distance(to: ~a) == 8)
		XCTAssert(a.maximumDistance == 8)
		let c: UInt16 = 0b0000_1111_0000_1111
		let d: UInt16 = 0b0011_1111_1100_1111
		XCTAssert(c.distance(to: d) == 4)
		XCTAssert(d.distance(to: c) == 4)
		XCTAssert(UInt8.isFixedWidth)
		XCTAssert(UInt16.isFixedWidth)
	}

	func testCollection()
	{
		let a: [UInt8] = [0xff, 0xf0, 0x00, 0x0f]
		let b: [UInt8] = [0x0f, 0x3c, 0x01, 0xf0]
		XCTAssert(a.maximumDistance == 32)
		XCTAssert(a.distance(to: b) == 4 + 4 + 1 + 8)
		XCTAssert(![UInt8].isFixedWidth)
		let c: [UInt8] = [0xff, 0xf0]
		XCTAssert(a.distance(to: c) == 16)
		XCTAssert(a.maximumDistance == 4 * 8)
	}
	func testNonFixedWidthUnequalLengths()
	{
		let a: [DNA] = [[0], [0, 0], [0, 0, 0]]
		let b: [DNA] = [[0], [0, 0]]
		let c: [DNA] = [[0], [0, 0], [0, 0, 0], [0, 0, 0, 0]]
		XCTAssert(a.distance(to: b) == 24)
		XCTAssert(a.distance(to: c) == 32)
		XCTAssert(b.distance(to: a) == 24)
		XCTAssert(c.distance(to: a) == 32)
		XCTAssert(a.maximumDistance == 6 * 8)
	}
}
