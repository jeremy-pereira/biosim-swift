//
//  AreaReducible.swift
//  
//
//  Created by Jeremy Pereira on 18/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


/// A protocol to adopt by two dimensional collections so we can use reduce on
/// areas within the collection
///
/// To conform to this protocol, implement ``clip`` and a subscript where the
/// index is of type ``Coordinate`` and the return value is of type `Element`
protocol AreaReducible
{
	/// Type of elements in the collection
	associatedtype Element
	/// Clips the given rect to have locations only in the collection
	///
	/// If the `Rect` is completely outside the grid, a `Rect` with the same
	/// bottom left but zero size is returned.
	/// - Parameter rect: The ``Rect`` to clip.
	/// - Returns: `rect` clipped to the grid.
	func clip(_ rect: Rect) -> Rect

	/// Subscript using a coordinate
	///
	/// - Parameters:
	///   - coordinate: the coordinate of the cell
	subscript(coordinate: Coordinate) -> Element { get }

}

extension AreaReducible
{
	/// Performs a reduction over the elements in the given radius from the location
	///
	/// This works like a normal reduce but on a region of elements in the
	/// collection. Note that the order of elements to the action is not guaranteed
	/// - Parameters:
	///   - location: the centre of the region
	///   - radius: how far from the centre to look. If zero, will only look at one location
	///   - initial: tje initial value for the reduction
	///   - action: What to do at each cell found. The coordinates and element
	///             are passed in to the closure with an intermediate T. The closure
	///             must return the next value of T.
	/// - Returns: The final T value
	/// - Todo: Potential crash if `radius` is not convertible to an `Int`
	func reduce<T>(location: Coordinate, radius: Double, initial: T, _ action: (Coordinate, Element, T) throws -> T) rethrows -> T
	{
		assert(radius >= 0)
		let rSquared = (radius * radius)
		let diagonalVector = Vector(Int(radius.rounded()), Int(radius.rounded()))
		let squareAroundLocation = Rect(bottomLeft: location - diagonalVector,
											 size: diagonalVector &* 2 &+ 1)
		let gridRect = self.clip(squareAroundLocation)
		var runningValue = initial
		for coordinate in gridRect
		{
			if Double((location - coordinate).length2) <= rSquared
			{
				runningValue = try action(coordinate, self[coordinate], runningValue)
			}
		}
		return runningValue
	}

}

/// A protocol to be adopted for a mutable visiting pattern
///
/// In addition to being ``AreaReducible`` this protocol has a ``visit`` function that can mutate the
/// elements it visits.
///
/// To implement, fulfill the requirements of ``AreaReducible`` and additionally implement a subscript
/// setter.
protocol MutableAreaVisitable: AreaReducible
{
	/// Subscript using a coordinate
	///
	/// - Parameters:
	///   - coordinate: the coordinate of the cell
	subscript(coordinate: Coordinate) -> Element { get set }
}

extension MutableAreaVisitable
{
	/// A visitor function to visit elements in a specific radius from a coordinate
	///
	/// Note that the order of calls to the action is not guaranteed
	/// - Parameters:
	///   - location: the centre of the region
	///   - radius: how far from the centre to look
	///   - action: What to do at each cell found. The coordinates and element
	///             are passed in to the closure. The element is an `inout` parameter
	/// - Returns: thre number of locations visited
	/// - Throws: If the closure called throws
	mutating func visit(location: Coordinate, radius: Double, _ action: (Coordinate, inout Element) throws -> ()) rethrows -> Int
	{
		assert(radius >= 0)
		let rSquared = (radius * radius)
		let diagonalVector = Vector(Int(radius.rounded()), Int(radius.rounded()))
		let squareAroundLocation = Rect(bottomLeft: location - diagonalVector,
											 size: diagonalVector &* 2 &+ 1)
		let gridRect = self.clip(squareAroundLocation)
		var count = 0
		for coordinate in gridRect
		{
			if Double((location - coordinate).length2) <= rSquared
			{
				try action(coordinate, &self[coordinate])
				count += 1
			}
		}
		return count
	}

}
