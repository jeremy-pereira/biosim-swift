//
//  ParamManager.swift
//  
//
//  Created by Jeremy Pereira on 28/11/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

private let log = Logger.getLogger("biosim-swift.ParamManager").with(level: .info)

/// Structure that manages parameters for the simulation
///
/// This struct uses dynamic member lookup to get the parameters. However,
/// because we might want to throw an exception, the dynamic member actually
/// returns a function that returns the member value or throws an exception.
/// As an example `ParamManager().sizeX` is a function of type `() throws -> Int`
/// or `() throws -> String` depending on the type of variable to which you will
/// assign the value. You need code like the following.
/// ```
/// let x: Int = try ParamManager().sizeX()    // x is assigned 128
/// let y: String = try ParamManager().sizeY() // y is assigned "128"
/// ```
@dynamicMemberLookup
public struct ParamManager
{
	/// Initialise the parameters with the content of the config file
	/// - Parameter fileName: The name of the file to read
	/// - Throws: if the file does not exist or can't be parsed
	public init(fileName: String) throws
	{
		log.warn("TODO: ParamManager ignores the init file")
		try self.init(parameters: [:])
	}

	/// Initialise parameters overriding the values given
	///
	/// Each parameter has a default value which can be overridden by the passed
	/// in dictionary.
	/// - Parameter parameters: Override default parameters
	public init(parameters: [String : ParamType]) throws
	{
		for aParam in parameters
		{
			self.parameters[aParam.key] = aParam.value
		}
		try consistencyCheck()
	}

	/// Initialise with the default parameters
	public init() throws
	{
		try self.init(parameters: [:])
	}

	private func consistencyCheck() throws
	{
		let displayScale: Int = try self.displayScale()
		let sizeX: Int = try self.sizeX()
		let sizeY: Int = try self.sizeY()
		guard sizeX * displayScale <= Int(UInt32.max)
		else
		{
			throw Simulation.Error.gridOrDisplayScaleTooLarge(dimension: "x",
															  size: sizeX,
															  scale: displayScale)
		}
		guard sizeY * displayScale <= Int(UInt32.max)
		else
		{
			throw Simulation.Error.gridOrDisplayScaleTooLarge(dimension: "y",
															  size: sizeY,
															  scale: displayScale)
		}
	}

	/// Types for parameters
	///
	/// These are the types you can have for a parameter. Typically, you can
	/// convert freely bewtween them as long as the conversion works. For example:
	/// `.string("1")` can be read as an integer from a ``ParamManager``
	/// The tyoe can be initialised by string, int, boolean and floating point
	/// literals and, in each case, you will get the relevant case.
	public enum ParamType
	{
		/// Integer parameter
		case int(Int)
		/// Double precision floating point parameter
		case float(Double)
		/// String parameter
		case string(String)
		/// Boolean parameter
		case bool(Bool)
		/// Reference to another parameter
		///
		/// The string represents the name of the other parameter and this
		/// parameter hads the same value as it.
		case reference(String)
	}

	private var parameters: [String : ParamType] = [
		"sizeX" : 128,
		"sizeY" : .reference("sizeX") /* Makes it square */,
		"signalLayers" : 1,
		"population" : 100,
		"replaceBarrierTypeGenerationNumber" : -1,
		"replaceBarrierType" : .int(Grid.BarrierType.none.rawValue),
		"barrierType" : .int(Grid.BarrierType.none.rawValue),
		"genomeInitialLengthMin" : 24,
		"genomeInitialLengthMax" : .reference("genomeInitialLengthMin"),
		"longProbeDistance" : 24,
		"maxNumberNeurons" : 5,
		"maxGenerations" : 200_000,
		"stepsPerGeneration" : 300,
		"videoStride" : 25,
		"genomeAnalysisStride" : .reference("videoStride"),
		"displaySampleGenomes" : 5,
		"populationSensorRadius" : 2.5,
		"signalSensorRadius" : 2.0,
		"shortProbeBarrierDistance" : 4,
		"enableKillForward" : false,
		"responsivenessCurveFactor" : 2,
		"challenge" : "cornerWeighted",
		"sexualReproduction" : true,
		"chooseParentsByFitness" : true,
		"pointMutationRate" : 0.00025,
		"saveVideo" : true,
		"videoSaveFirstFrames" : 2,
		"displayScale" : 8,
		"imageDir" : "images",
		"agentSize" : 4,
	]
	/// Subscript that returns a function that extracts a parameter as a `String`
	///
	/// The subscript returns a function that must be called to retrieve the
	/// actual parameter value. This is necessary because a dynamic member
	/// lookup can't throw
	/// - Parameters:
	///   - member: The parameter key
	/// - Returns: A function that can be called to get the value for the
	///            parameter
	/// - Throws: If the parameter does not exist
	public subscript(dynamicMember member: String) -> () throws -> String
	{
		get
		{
			{
				guard let ret = parameters[member]
				else
				{
					throw Simulation.Error.missingParameter(member)
				}
				return try toString(value: ret)
			}
		}
	}

	/// Subscript that returns a function that extracts a parameter as an `Int`
	///
	/// The subscript returns a function that must be called to retrieve the
	/// actual parameter value. This is necessary because a dynamic member
	/// lookup can't throw
	/// - Parameters:
	///   - member: The parameter key
	/// - Returns: A function that can be called to get the value for the
	///            parameter
	/// - Throws: If the parameter does not exist or cannot be converted to `Int`
	public subscript(dynamicMember member: String) -> () throws -> Int
	{
		get
		{
			{
				guard let ret = parameters[member]
				else
				{
					throw Simulation.Error.missingParameter(member)
				}
				return try toInt(value: ret, name: member)
			}
		}
	}

	/// Subscript that returns a function that extracts a parameter as a `Double`
	///
	/// The subscript returns a function that must be called to retrieve the
	/// actual parameter value. This is necessary because a dynamic member
	/// lookup can't throw
	/// - Parameters:
	///   - member: The parameter key
	/// - Returns: A function that can be called to get the value for the
	///            parameter
	/// - Throws: If the parameter does not exist or cannot be converted to `Double`
	public subscript(dynamicMember member: String) -> () throws -> Double
	{
		get
		{
			{
				guard let ret = parameters[member]
				else
				{
					throw Simulation.Error.missingParameter(member)
				}
				return try toDouble(value: ret, name: member)
			}
		}
	}
	/// Subscript that returns a function that extracts a parameter as an `Bool`
	///
	/// The subscript returns a function that must be called to retrieve the
	/// actual parameter value. This is necessary because a dynamic member
	/// lookup can't throw
	/// - Parameters:
	///   - member: The parameter key
	/// - Returns: A function that can be called to get the value for the
	///            parameter
	/// - Throws: If the parameter does not exist or cannot be converted to `Bool`
	public subscript(dynamicMember member: String) -> () throws -> Bool
	{
		get
		{
			{
				guard let ret = parameters[member]
				else
				{
					throw Simulation.Error.missingParameter(member)
				}
				return try toBool(value: ret, name: member)
			}
		}
	}

	/// Subscript that returns a function that extracts a parameter as a
	/// ``Grid.BarrierType``
	///
	/// The subscript returns a function that must be called to retrieve the
	/// actual parameter value. This is necessary because a dynamic member
	/// lookup can't throw
	/// - Parameters:
	///   - member: The parameter key
	/// - Returns: A function that can be called to get the value for the
	///            parameter
	/// - Throws: If the parameter does not exist or cannot be converted to ``Grid.BarrierType``
	public subscript(dynamicMember member: String) -> () throws -> Grid.BarrierType
	{
		get
		{
			{
				let retAsInt: Int = try self[dynamicMember: member]()
				guard let ret = Grid.BarrierType(rawValue: retAsInt)
				else { throw Simulation.Error.parameterConversionFailed(member,
																		destType: "barrier",
																		value: retAsInt.description)}
				return ret
			}
		}
	}

	func getBool(key: String, defaultIfMissing: Bool = false) throws -> Bool
	{
		guard let ret = parameters[key] else { return defaultIfMissing }
		return try toBool(value: ret, name: key)
	}

	private func toInt(value: ParamType, name: String) throws -> Int
	{
		switch value
		{
		case .int(let value):
			return value
		case .string(let valueAsString):
			guard let value = Int(valueAsString)
			else
			{
				throw Simulation.Error.parameterConversionFailed(name, destType: "Int", value: valueAsString)
			}
			return value
		case .float(let value):
			guard (Double(Int.min) ... Double(Int.max)).contains(value)
			else { throw Simulation.Error.parameterConversionFailed(name, destType: "Int", value: "\(value)") }
			return Int(value.rounded())
		case .bool(let value):
			throw Simulation.Error.parameterConversionFailed(name, destType: "Int", value: "\(value)")
		case .reference(let pName):
			return try self[dynamicMember: pName]()
		}
	}

	private func toDouble(value: ParamType, name: String) throws -> Double
	{
		switch value
		{
		case .int(let value):
			return Double(value)
		case .string(let valueAsString):
			guard let value = Double(valueAsString)
			else
			{
				throw Simulation.Error.parameterConversionFailed(name, destType: "Double", value: valueAsString)
			}
			return value
		case .float(let value):
			return value
		case .bool(let value):
			throw Simulation.Error.parameterConversionFailed(name, destType: "Double", value: "\(value)")
		case .reference(let pName):
			return try self[dynamicMember: pName]()
		}
	}

	private func toString(value: ParamType) throws -> String
	{
		switch value
		{
		case .int(let int):
			return "\(int)"
		case .float(let value):
			return ("\(value)")
		case .string(let string):
			return string
		case .bool(let value):
			return "\(value)"
		case .reference(let pName):
			return try self[dynamicMember: pName]()
		}
	}

	private func toBool(value: ParamType, name: String) throws -> Bool
	{
		switch value
		{
		case .int(let value):
			throw Simulation.Error.parameterConversionFailed(name, destType: "Bool", value: "\(value)")
		case .float(let value):
			throw Simulation.Error.parameterConversionFailed(name, destType: "Bool", value: "\(value)")
		case .string(let value):
			switch value.lowercased()
			{
			case "true": return true
			case "false": return false
			default:
				throw Simulation.Error.parameterConversionFailed(name, destType: "Bool", value: value)
			}
		case .bool(let value):
			return value
		case .reference(let pName):
			return try self[dynamicMember: pName]()
		}
	}
}

extension ParamManager.ParamType:
	ExpressibleByStringLiteral, ExpressibleByIntegerLiteral,
	ExpressibleByFloatLiteral, ExpressibleByBooleanLiteral
{
	public init(stringLiteral: String)
	{
		self = ParamManager.ParamType.string(stringLiteral)
	}

	public init(integerLiteral: Int)
	{
		self = .int(integerLiteral)
	}

	public init(floatLiteral: Double)
	{
		self = .float(floatLiteral)
	}

	public init(booleanLiteral: Bool)
	{
		self = .bool(booleanLiteral)
	}
}
