//
//  ImageWriter.swift
//  
//
//  Created by Jeremy Pereira on 02/01/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox
import EvoKit

private let log = Logger.getLogger("biosim-swift.ImageWriter")

/// Used to write images from the video
///
/// This will probably end up being an `actor` so it can handle the writing
/// asynchronously
/// - Todo: This may become just a protocol
public struct ImageWriter
{
	var delegate: ImageWriterDelegate

	init(delegate: ImageWriterDelegate)
	{
		self.delegate = delegate
	}

	/// Synchronously save a video frame
	/// - Parameters:
	///   - simStep: The step of the simulation we are in
	///   - generation: The generation we are on
	///   - individuals: Individuals in the grid
	///   - barrierLocations: Locations of all barriers
	///   - gridDimensions: How big is the grid
	mutating func saveVideoFrameSync<IS: Collection>(simStep: Int, generation: Int, individuals: IS, signals: Signals, grid: Grid)
	where IS.Element == Individual
	{

		let frame = FrameData(simStep: simStep, generation: generation, individuals: individuals, signals: signals, grid: grid)
		delegate.saveOneFrame(frame: frame)
	}

	mutating func saveVideo(generation: Int)
	{
		delegate.saveVideo(generation: generation)
	}
}

public extension ImageWriter
{
	/// A type that encapsulates basic RGB data
	typealias RGB = (red: UInt8, green: UInt8, blue: UInt8)
	struct FrameData
	{
		/// The step of the simulation
		public let simStep: Int
		/// The generation of evolution
		public let generation: Int
		/// locations and colours of the individuals
		public let individualData: [(location: Coordinate, colour: RGB)]
		/// Signal layer information
		public let signals: Signals
		public let barrierLocations: [Coordinate]
		/// Dimensions of the grid
		public let dimensions: Rect

		init<IS: Collection>(simStep: Int, generation: Int, individuals: IS, signals: Signals, grid: Grid)
		where IS.Element == Individual
		{
			self.simStep = simStep
			self.generation = generation
			self.signals = signals
			self.barrierLocations = grid.barrierLocations
			self.dimensions = grid.rect
			self.individualData = individuals.map
			{
				(location: $0.location, colour: FrameData.makeColour(individual: $0))
			}
		}

		static private func makeColour(individual: Individual) -> RGB
		{
			// Simply xor the bases together rotating between red green and blue
			// TODO: Think of a better colour algorithm
			let dna = individual.dna
			var rgb: [UInt8] = [0, 0, 0]

			var dnaIterator = dna.makeIterator()
			var currentIndex = 0
			while let value = dnaIterator.next()
			{
				rgb[currentIndex] ^= value
				currentIndex = (currentIndex + 1) % rgb.count
			}
			return (red: rgb[0], green: rgb[1], blue: rgb[2])
		}
	}

}


/// Conform to this delegate to write images for movies or other purposes
public protocol ImageWriterDelegate
{
	/// Save an image frame
	///
	/// Can do what you like with a frame e.g. draw in a window or save to a
	/// file.
	/// - Parameters:
	///   - frame: Frame data to save
	mutating func saveOneFrame(frame: ImageWriter.FrameData)

	/// Save a video for the simulation for a generation
	///
	/// - Parameters:
	///   - generation: The generation for the current video
	mutating func saveVideo(generation: Int)
}

/// Image writer delegate for debugging
///
/// Writes lots of debug messages and not much else
public struct DebugDelegate: ImageWriterDelegate
{
	public mutating func saveVideo(generation: Int)
	{
		log.debug("Should save video for generation \(generation)")
	}

	public func saveOneFrame(frame: ImageWriter.FrameData)
	{
		log.debug("Got frame for generation \(frame.generation), step \(frame.simStep)")
	}
}
