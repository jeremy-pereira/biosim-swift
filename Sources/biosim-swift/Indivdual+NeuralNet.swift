//
//  Individual+NeuralNet.swift
//  
//
//  Created by Jeremy Pereira on 11/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

extension Individual
{

	/// Neural net for an individual
	struct NeuralNet
	{
		/// A neuron
		struct Neuron
		{
			var output: Double
			var driven: Bool

			static let initialOutput = 0.5
		}

		/// Connections between sensors, neurons and actions
		private(set) var connections: [Genome.Gene]
		/// THe neurons in this neural net
		private(set) var neurons: [Neuron]

		/// Initialise an empty neural net
		init()
		{
			connections = []
			neurons = []
		}

		/// Initialise a neural net from a genome
		/// - Parameters:
		///   - genome: The genome from which to initialise this neural net
		///   - maxNeurons: The mx number of neurons allowed in the neural net
		init(genome: Genome, maxNeurons: Int)
		{
			var connectionList = NeuralNet.makeConnectionList(maxNeurons: maxNeurons, genome: genome)
			var nodeMap = NeuralNet.makeNodeList(from: connectionList)
			NeuralNet.cullUselessNeurons(connections: &connectionList, nodeMap: &nodeMap)

			// Renumber the neurons that are left, starting from zero

			var newNumber: UInt16 = 0
			for key in nodeMap.keys
			{
				var node = nodeMap[key]!
				assert(node.numOutputs > 0) // Just a sanity check
				node.remappedNumber = newNumber
				nodeMap[key] = node
				newNumber += 1
			}

			// Build the connections. We do connnections to neurons first, then
			// connections to actions because it optimises the feed forward function
			// For each neuron in each connection, we remap the index.

			let connectionsToNeurons: [Genome.Gene] = connectionList
				.filter{ $0.sinkIsNeuron }
				.map
				{
					var adjustedConnection = $0
					adjustedConnection.sink = nodeMap[$0.sink]!.remappedNumber
					if $0.sourceIsNeuron
					{
						adjustedConnection.source = nodeMap[$0.source]!.remappedNumber
					}
					return adjustedConnection
				}
			let connectionsToActions: [Genome.Gene] = connectionList
				.filter { $0.sinkIsAction }
				.map
				{
					var adjustedConnection = $0
					if $0.sourceIsNeuron
					{
						adjustedConnection.source = nodeMap[$0.source]!.remappedNumber
					}
					return adjustedConnection
				}
			self.connections = connectionsToNeurons + connectionsToActions

			//
			// Now create the neurons

			neurons = nodeMap.values
				.sorted{ $0.remappedNumber < $1.remappedNumber }
				.map{ Neuron(output: Neuron.initialOutput, driven: $0.numInputsFromOtherNodes > 0) }
		}

		private typealias NodeMap = [ UInt16 : Node]
		private struct Node
		{
			var remappedNumber: UInt16 = 0
			var numOutputs = 0
			var numSelfInputs = 0
			var numInputsFromOtherNodes = 0
		}

		/// Set and normalise the neuron output values
		///
		/// We only optimise the driven neurons. Undriven ones don't change
		/// their values by definition.
		/// - Parameters:
		///   - values: values to normalise the neurons to
		///   - normalise: Function that should normalise the neuron values
		///                to the range `-1 ... 1`
		mutating func latchNeurons(to values: [Double], normalise: (Double) -> Double)
		{
			for i in 0 ..< neurons.count
			{
				if neurons[i].driven
				{
					neurons[i].output = normalise(values[i])
				}
			}
		}

		// MARK: Initialistion from a genome

		 /// Make a list of connections but with the nodes renumbered
		 ///
		 /// Internal neurons are renumbered modulo the maximum number of neurons
		 /// allowed. Sensors and actions are renumbered mudulo the number of sensors
		 /// and actions
		 /// - Parameter maxNeurons: Maximum number of internal neurons
		 /// - Returns: A connection list
		 private static func makeConnectionList(maxNeurons: Int, genome: Genome) -> ConnectionList
		 {
			 var ret = ConnectionList()
			 genome.forEach
			 {
				 var gene = $0
				 if gene.sourceIsSensor
				 {
					 gene.source %= UInt16(Sensor.allCases.count)
				 }
				 else
				 {
					 gene.source %= UInt16(maxNeurons)
				 }
				 if gene.sinkIsAction
				 {
					 gene.sink %= UInt16(Action.allCases.count)
				 }
				 else
				 {
					 gene.sink %= UInt16(maxNeurons)
				 }
				 ret.append(gene)
			 }
			 return ret
		 }

		 private static func makeNodeList(from connectionList: ConnectionList) -> NodeMap
		 {
			 var nodeMap = NodeMap()

			 for gene in connectionList
			 {
				 if gene.sinkIsNeuron
				 {
					 // The sink end of the connection is a neuron
					 var node: Node
					 if let found = nodeMap[gene.sink]
					 {
						 node = found
					 }
					 else
					 {
						 node = Node()
					 }
					 if gene.sourceIsNeuron && gene.source == gene.sink
					 {
						 // Self connection
						 node.numSelfInputs += 1
					 }
					 else
					 {
						 // Not a connection to self
						 node.numInputsFromOtherNodes += 1
					 }
					 nodeMap[gene.sink] = node
				 }
				 if gene.sourceIsNeuron
				 {
					 // The source end of the connection is a neuron
					 var node: Node
					 if let found = nodeMap[gene.source]
					 {
						 node = found
					 }
					 else
					 {
						 node = Node()
					 }
					 node.numOutputs += 1
					 nodeMap[gene.source] = node
				 }
			 }
			 return nodeMap
		 }

		 private static func cullUselessNeurons(connections: inout ConnectionList, nodeMap: inout NodeMap)
		 {
			 var allDone = false
			 while !allDone
			 {
				 allDone = true
				 let indexes = nodeMap.keys
				 for index in indexes
				 {
					 let node = nodeMap[index]!
					 // Any neuron that feeds only itself or nothing at all must go
					 assert(node.numOutputs >= node.numSelfInputs)
					 if node.numOutputs == node.numSelfInputs
					 {
						 allDone = false
						 removeConnectionsToNeuron(connections: &connections, nodeMap: &nodeMap, index: index)
						 nodeMap[index] = nil
					 }
				 }
			 }
		 }

		 private static func removeConnectionsToNeuron(connections: inout ConnectionList,
													   nodeMap: inout NodeMap,
													   index: UInt16)
		 {
			 var indexesToDelete = Stack<Int>()
			 for i in connections.startIndex ..< connections.endIndex
			 {
				 if connections[i].sinkIsNeuron && connections[i].sink == index
				 {
					 // This connection must go
					 if connections[i].sourceIsNeuron
					 {
						 // If it comes from a neuron, that neurons outputs goes down
						 guard var sourceNode = nodeMap[connections[i].source]
						 else { fatalError("Missing neuron from node map") }
						 sourceNode.numOutputs -= 1
						 nodeMap[connections[i].source] = sourceNode
					 }
					 indexesToDelete.push(i)
				 }
			 }
			 while let indexToDelete = indexesToDelete.pop()
			 {
				 connections.remove(at: indexToDelete)
			 }
		 }
	}
}

extension Array where Element == Individual.NeuralNet.Neuron
{
	/// A convenience subscript for arrays of neurons
	subscript(position: UInt16) -> Individual.NeuralNet.Neuron
	{
		get { self[Int(position)] }
	}
}
