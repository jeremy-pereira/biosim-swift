//
//  BasicTypes.swift
//  
//
//  Created by Jeremy Pereira on 27/11/2021.
//

import Darwin
import EvoKit

/// Enumeration representing a direction or no direction (case `centre`)
public enum Direction: Int8, CaseIterable
{
	case north = 0
	case northEast
	case east
	case southEast
	case south
	case southWest
	case west
	case northWest
	case centre		// Must be the last direction

	/// The raw value as an `Int` as per biosim4
	///
	/// Our enum is logically numbered. The enum in Biosim4 is not. This function gets the correct
	/// Biosim4 integer
	/// - Returns: `rawValue` converted to an `Int` compatible with Biosim4
	public func asInt() -> Int
	{
		switch self
		{
		case .north:
			return 7
		case .northEast:
			return 8
		case .east:
			return 5
		case .southEast:
			return 2
		case .south:
			return 1
		case .southWest:
			return 0
		case .west:
			return 3
		case .northWest:
			return 6
		case .centre:
			return 4
		}
	}

	/// Rotate the direction by a number of increments.
	/// - Parameter count: How many increments to rotate
	/// - Returns: the rotated direction
	public func rotated(_ count: Int) -> Direction
	{
		guard self != .centre else { return .centre }
		let moddedCount = Int8(count % Int(Direction.realDirectionCount))
		let normalisedCount = (count >= 0 ? 0 : Direction.realDirectionCount) + moddedCount
		return Direction(rawValue: (self.rawValue + normalisedCount) % Direction.realDirectionCount)!
	}

	/// The number of directions that actually go somewhere
	public static let realDirectionCount = Direction.centre.rawValue

	/// The normalised vector for this direction
	///
	/// The normalised vector is the vector that has `x` and `y` selected from
	/// the set *{ -1, 0, 1 }*. This is not the same as a mathematical
	/// normalised vector.
	/// - Todo: Possibly calculate on `init` because instances are immutable
	public var normalisedVector: Vector
	{
		switch self
		{

		case .north:
			return Vector(0, 1)
		case .northEast:
			return Vector(1, 1)
		case .east:
			return Vector(1, 0)
		case .southEast:
			return Vector(1, -1)
		case .south:
			return Vector(0, -1)
		case .southWest:
			return Vector(-1, -1)
		case .west:
			return Vector(-1, 0)
		case .northWest:
			return Vector(-1, 1)
		case .centre:
			return Vector(0, 0)
		}
	}

	/// A unit vector in the direction
	///
	/// This is mathmatically normalised so that the length of the vector is 1
	public var unitSIMD: SIMD2<Double>
	{
		let sqrt2 = 2.0.squareRoot()

		switch self
		{

		case .north:
			return SIMD2(0, 1)
		case .northEast:
			return SIMD2(sqrt2, sqrt2)
		case .east:
			return SIMD2(1, 0)
		case .southEast:
			return SIMD2(sqrt2, -sqrt2)
		case .south:
			return SIMD2(0, -1)
		case .southWest:
			return SIMD2(-sqrt2, -sqrt2)
		case .west:
			return SIMD2(-1, 0)
		case .northWest:
			return SIMD2(-sqrt2, sqrt2)
		case .centre:
			return SIMD2(0, 0)
		}
	}

	/// This direction as a polar with a magnitude of 1.
	///
	/// - Todo: Possibly calculate on `init` because instances are immutable
	public var normalisedPolar: Polar
	{
		Polar(magnitude: self == .centre ? 0 : 1, direction: self)
	}

	static let compassPoints: [Direction] = [.north, .northEast, .east, .southEast, .south, .southWest, .west, .northWest]

	static func random() -> Direction
	{
		return compassPoints.randomElement(using: &EvoKit.rng)!
	}
}

/// An integer based vector
public struct Vector: Equatable
{
	internal let value: SIMD2<Int>

	/// Create a vector from a pair of integer coordinates
	/// - Parameters:
	///   - x: Magnitude in the *x* direction (negative for vectors going west)
	///   - y: Magnitude in the *y* direction (negative for vectors going south)
	public init(_ x: Int, _ y: Int)
	{
		self.init(SIMD2(x, y))
	}

	init(_ value: SIMD2<Int>)
	{
		self.value = value
	}

	public var x: Int { value.x }
	public var y: Int { value.y }

	/// True if this vector is normalised
	///
	/// "Normalised" means that both `x` and `y` are in the set *{ -1, 0, 1 }*.
	/// Note that, by this definition (0, 0) is normalised.
	public var isNormalised: Bool
	{
		(-1 ... 1).contains(x) && (-1 ... 1).contains(y)
	}
	/// The direction of this vector.
	///
	/// We only have the eight compass points, so the direction is rounded to
	/// the nearest one. For example, the direction of (0, 100) is `.north`
	public var direction: Direction
	{
		guard !(x == 0 && y == 0) else { return Direction.centre }

		// We need to convert the angle of the vector from normal maths angles
		// which go from the x axis anticlockwise to Direction raw values
		// which go clockwise from north. This is why the parameters are
		// swapped (normally, y is first)
		let theta: Double
		if x < 0
		{
			theta = 2 * Double.pi + atan2(Double(x), Double(y))
		}
		else
		{
			theta = atan2(Double(x), Double(y))
		}
		assert((0 ..< (2 * Double.pi)).contains(theta), "Expected angle '\(theta)' to be in range [0, 2π)")
		// Now we need to round to the nearest π/4.
		let eighthCircles = (theta + Double.pi / 8) / (Double.pi / 4)
		let compassNumber = Int8(eighthCircles) % Direction.realDirectionCount
		assert((0 ..< Direction.realDirectionCount).contains(compassNumber), "Got the rounding wrong")
		return Direction(rawValue: compassNumber)!
	}

	/// A normalised version of this vector.
	public var normalised: Vector
	{
		guard !self.isNormalised else { return self }
		return direction.normalisedVector
	}


	/// Length of the vector to the nearest `Int`
	public var length: Int
	{
		let square = Double((value &* value).wrappedSum())
		let length = square.squareRoot()
		return Int(length.rounded())
	}
	///Square of the length of the vector
	public var length2: Int
	{
		(value &* value).wrappedSum()
	}

	public static func == (lhs: Vector, rhs: Vector) -> Bool
	{
		lhs.value == rhs.value
	}

	public static func &*(lhs: Vector, rhs: Int) -> Vector
	{
		Vector(lhs.value &* rhs)
	}

	public static func &*(lhs: Vector, rhs: Vector) -> Vector
	{
		Vector(lhs.value &* rhs.value)
	}

	public static func &+(lhs: Vector, rhs: Int) -> Vector
	{
		Vector(lhs.value &+ rhs)
	}

	public static func /(lhs: Vector, rhs: Int) -> Vector
	{
		Vector(lhs.value / rhs)
	}


	/// Unit vector along the x axis
	public static let i = Vector(1, 0)
	/// Unit vector along the y axis
	public static let j = Vector(0, 1)
}

/// A type representing a polar coordinate
///
/// Has a magnitude and a direction. Direction can only be one of the eight
/// compass points. Be wary of magnitude because we using an integer grid
public struct Polar
{
	public let magnitude: Int
	/// THe direction is a compass point as defined by ``Direction``
	public let direction: Direction

	/// Initialise a `Polar`
	/// - Parameters:
	///   - magnitude: The magnitude
	///   - direction: the direction
	init(magnitude: Int, direction: Direction)
	{
		self.magnitude = magnitude
		self.direction = direction
	}
}

/// Represents coordinates in a two dimensional collection
public struct Coordinate
{
	private let value: SIMD2<Int>
	/// The x coordinate
	public var x: Int { value.x }
	/// The y coordinate
	public var y: Int { value.y }

	public init(_ x: Int, _ y: Int)
	{
		let value = SIMD2(x, y)
		self.init(value: value)
	}

	private init(value: SIMD2<Int>)
	{
		self.value = value
	}

	/// Adding a vector to a coordinate gives another coordinate
	/// - Parameters:
	///   - lhs: The coordinate to add
	///   - rhs: The vector to add to it
	///  - Returns: The result of translating trhe coordinate by the vector
	public static func + (lhs: Coordinate, rhs: Vector) -> Coordinate
	{
		return Coordinate(value: lhs.value &+ rhs.value)
	}

	public static func += (lhs: inout Coordinate, rhs: Vector)
	{
		lhs = Coordinate(value: lhs.value &+ rhs.value)
	}

	public static func - (lhs: Coordinate, rhs: Coordinate) -> Vector
	{
		Vector(lhs.value &- rhs.value)
	}

	public var positionVector: Vector { self - Coordinate.origin }

	public static func - (lhs: Coordinate, rhs: Vector) -> Coordinate
	{
		Coordinate(value: lhs.value &- rhs.value)
	}
	/// The origin at `(0, 0)`
	public static let origin = Coordinate(0, 0)
}

extension Coordinate: CustomStringConvertible
{
	public var description: String { "(\(x), \(y))"}
}

extension Coordinate: Equatable
{
	public static func == (lhs: Coordinate, rhs: Coordinate) -> Bool
	{
		lhs.value == rhs.value
	}
}

/// A rectangular region of a grid
public struct Rect: Collection
{
	let origin: Coordinate
	let size: Vector

	var rightX: Int { origin.x + size.x }
	var topY: Int { origin.y + size.y }

	public var topRight: Coordinate { origin + size }
	public var topLeft: Coordinate { origin + Vector(0, size.y) }
	public var bottomRight: Coordinate { origin + Vector(size.x, 0) }

	public init(bottomLeft: Coordinate, size: Vector)
	{
		guard size.x >= 0 && size.y >= 0
		else { fatalError("Cannot have a rectangle with a negative size") }
		self.origin = bottomLeft
		self.size = size
		endIndex = size.x * size.y
	}

	// MARK: Rect Collection conformance

	public let startIndex = 0
	public let endIndex: Int

	public subscript(position: Int) -> Coordinate
	{
		let y = position / size.x + origin.y
		let x = position % size.x + origin.x
		return Coordinate(x, y)
	}

	public func index(after i: Int) -> Int
	{
		i + 1
	}

	public func contains(_ element: Coordinate) -> Bool
	{
		(origin.x ..< rightX).contains(element.x)
		&& (origin.y ..< topY).contains(element.y)
	}
}

extension Rect: Equatable
{
	/// Equality of two `Rect`s
	/// - Returns: true if the rects both have the same origin and size
	public static func == (lhs: Rect, rhs: Rect) -> Bool
	{
		lhs.origin == rhs.origin && lhs.size == rhs.size
	}
}
