//
//  Individual.swift
//  
//
//  Created by Jeremy Pereira on 05/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Toolbox
import Darwin
import EvoKit

private let log = Logger.getLogger("biosim-swift.Individual")

/// An individual in the simulation
struct Individual
{
	typealias ConnectionList = [Genome.Gene]

	/// The maximum long probe distance allowed for an individual.
	static let maxLongProbeDistance = 32
	/// Threshold above which kills can occur for ``Action.killForward``
	static let killThreshold = 0.5

	let index: Int
	let dna: DNA
	private(set) var location: Coordinate
	var age = 0
	private var oscPeriod = 34	// TODO: get rid of the magic number
	private(set) var alive = true
	private(set) var lastMoveDir = Direction.random()
	private(set) var responsiveness = 0.5
	private(set) var longProbeDistance: Int
	/// Challenge bits are set when the individual has accomplished some task
	private(set) var challengebits: UInt32 = 0
	private(set) var neuralNet: NeuralNet

	init()
	{
		index = 0
		dna = DNA()
		location = Coordinate(0, 0)
		longProbeDistance = 0
		neuralNet = NeuralNet()
	}

	init(index: Int,
		 location: Coordinate,
		 dna: DNA,
		 longProbeDistance: Int,
		 maxNumberNeurons: Int,
		 onCreate: (Individual) throws -> () = { _ in }) rethrows
	{
		self.dna = dna
		self.index = index
		self.location = location
		self.longProbeDistance = longProbeDistance
		neuralNet = NeuralNet(genome: Genome(dna: dna), maxNeurons: maxNumberNeurons)
		try onCreate(self)
	}

	mutating func kill() { alive = false }

	mutating func feedForward(step: Int, environment: SensorEnvironment) -> ActionLevels
	{
		var actionLevels = ActionLevels()
		var neuronAccumulators = Array<Double>(repeating: 0, count: neuralNet.neurons.count)
		var neuronOutputComputed = false

		for conn in neuralNet.connections
		{
			if conn.sinkIsAction && !neuronOutputComputed
			{

				// The actions come after everything else, so now we update
				// and latch the neuron outputs to their proper ranges.
				neuralNet.latchNeurons(to: neuronAccumulators, normalise: tanh)
				neuronOutputComputed = true
			}
			assert(conn.sinkIsAction == neuronOutputComputed, "Actions must be at the end of the connection list")

			// Find the input based on whether it is from a sensor or a neuron

			let inVal: Double
			if conn.sourceIsSensor
			{
				inVal = getSensor(Sensor(rawValue: conn.source)!, step: step, environment: environment)
			}
			else
			{
				inVal = neuralNet.neurons[conn.source].output
			}

			// Weight the input value and add to the correct accumulator

			if conn.sinkIsAction
			{
				let action = Action(rawValue: conn.sink)!
				actionLevels[action] += inVal * conn.weightAsDouble
			}
			else
			{
				neuronAccumulators[Int(conn.sink)] += inVal * conn.weightAsDouble
			}
		}
		return actionLevels
	}

	mutating func move(to newLocation: Coordinate)
	{
		lastMoveDir = (newLocation - location).direction
		location  = newLocation
	}

	private func getSensor(_ sensor: Sensor, step: Int, environment: SensorEnvironment) -> Double
	{
		// Start with a sanity check to make sure the individual is not lying
		// about where it is.
		assert(self.location == environment.location(of: self.index)!, "Individual is lost in the grid")

		let sensorVal: Double
		switch sensor
		{
		case .locX:
			let gridSizeX = environment.gridSize.x
			sensorVal = gridSizeX > 1 ? Double(location.x) / Double(gridSizeX - 1) : 0
		case .locY:
			let gridSizeY = environment.gridSize.y
			sensorVal = gridSizeY > 1 ? Double(location.x) / Double(gridSizeY - 1) : 0
		case .boundaryDistX:
			let gridSize = environment.gridSize
			let distX = min(location.x, gridSize.x - location.x - 1)
			let maxPossible = (gridSize.x + 1) / 2 - 1
			sensorVal = maxPossible > 0 ? Double(distX) / Double(maxPossible)
										: 0
		case .boundaryDist:
			let gridSize = environment.gridSize
			let distX = min(location.x, gridSize.x - location.x - 1)
			let distY = min(location.y, gridSize.y - location.y - 1)
			let closest = min(distX, distY)
			let maxPossible = max((gridSize.x + 1) / 2 - 1, (gridSize.y + 1) / 2 - 1)
			sensorVal = maxPossible > 0 ? Double(closest) / Double(maxPossible)
										: 0
		case .boundaryDistY:
			let gridSize = environment.gridSize
			let distY = min(location.y, gridSize.y - location.y - 1)
			let maxPossible = (gridSize.y + 1) / 2 - 1
			sensorVal = maxPossible > 0 ? Double(distY) / Double(maxPossible)
			: 0
		case .geneticSimFwd:
			let loc2 = location + lastMoveDir.normalisedVector
			// Reducing with a radius of 0 gives us just one location
			sensorVal = environment.reduce(location: loc2, radius: 0, initial: Double(0))
			{
				_, cell, _ in
				switch cell
				{
				case .individual(let index):
					let other = environment.peeps[index]
					return self.dna.similarity(to: other.dna)
				default:
					return 0
				}
			}
		case .lastMoveDirX:
			let x = lastMoveDir.normalisedVector.x
			sensorVal = Double(x + 1) / 2
		case .lastMoveDirY:
			let y = lastMoveDir.normalisedVector.y
			sensorVal = Double(y + 1) / 2
		case .longProbePopFwd:
			if longProbeDistance > 0
			{
				let probeDistance = environment.longProbe(location: location,
														  direction: lastMoveDir,
														  type: .individual(0),
														  distance: longProbeDistance)
				sensorVal = probeDistance / Double(longProbeDistance)
			}
			else
			{
				sensorVal = 1
			}
		case .longProbeBarFwd:
			if longProbeDistance > 0
			{
				let probeDistance = environment.longProbe(location: location,
														  direction: lastMoveDir,
														  type: .barrier,
														  distance: longProbeDistance)
				sensorVal = probeDistance / Double(longProbeDistance)
			}
			else
			{
				sensorVal = 1
			}
		case .population:
			var occupiedCount = 0
			let totalCells = environment.visit(location: location, radius: environment.populationSensorRadius)
			{
				_, cell in
				if case Grid.Cell.individual(_) = cell
				{
					occupiedCount += 1
				}
			}
			assert(occupiedCount >= 1, "This individual at least should be in the region")
			sensorVal = totalCells > 0 ? Double(occupiedCount) / Double(totalCells) : 0
		case .populationFwd:
			sensorVal = environment.populationDensity(location: location, direction: lastMoveDir)
		case .populationLR:
			sensorVal = environment.populationDensity(location: location, direction: lastMoveDir.rotated(1))
		case .osc1:
			assert(oscPeriod > 0, "Invalid oscillation period")
			let phase = Double(step % oscPeriod) / Double(oscPeriod)
			let factor = cos(phase * 2 * Double.pi)
			assert((-1.0 ... 1.0).contains(factor), "cos doesn't do what I thought!")
			sensorVal = (factor + 1) / 2 // -1 ... 1 => 0 ... 1
		case .age:
			sensorVal = Double(age) / Double(environment.stepsPerGeneration)
		case .barrierFwd:
			if environment.shortProbeBarrierDistance > 0
			{
				sensorVal = environment.shortProbe(location: location,
												   direction: lastMoveDir,
												   type: .barrier,
												   distance: environment.shortProbeBarrierDistance)
			}
			else
			{
				sensorVal = 0.5
			}
		case .barrierLR:
			if environment.shortProbeBarrierDistance > 0
			{
				sensorVal = environment.shortProbe(location: location,
												   direction: lastMoveDir.rotated(1),
												   type: .barrier,
												   distance: environment.shortProbeBarrierDistance)
			}
			else
			{
				sensorVal = 0.5
			}
		case .random:
			sensorVal = Double.random(in: 0 ... 1, using: &EvoKit.rng)
		case .signal0:
			sensorVal = environment.signalDensity(layer: 0, location: location)
		case .signal0Fwd:
			sensorVal = environment.signalDensity(layer: 0, location: location, direction: lastMoveDir)
		case .signal0LR:
			sensorVal = environment.signalDensity(layer: 0, location: location, direction: lastMoveDir.rotated(1))
		}
		assert((0.0 ... 1.0).contains(sensorVal))
		// TODO: If the assertion fails, clamp sensorVal
		return sensorVal
	}

	/// Execute an action in a given environment
	///
	/// Note that there are several movement neurons and their effects must be
	/// treated cumulatively. So we don't actually perform the movemnt in here,
	/// we just do the adding up. The environment notes which direction we want
	/// to move and then performs the movement, if it is possible.
	/// - Parameters:
	///   - action: The action to execute
	///   - level: The input level of the action neuron
	///   - movement: A vector that accumulates the movement levels in the x and
	///               y directions.
	///   - environment: The environment in which to execute it
	mutating func execute(action: Action, level: Double, movement: inout SIMD2<Double>, environment: inout ActionEnvironment)
	{
		assert(environment.enabledActions.contains(action))

		log.debug("Individual \(index): Action = \(action), level = \(level), movement = \(movement)")

		switch action
		{
		case .setOscilatorPeriod:
			oscPeriod = 1 + Int(1.5 + exp(7 * level))
			assert((2 ... 2048).contains(oscPeriod))
		case .setLongProbeDist:
			longProbeDistance = Int(1 + level * Double(Individual.maxLongProbeDistance))
		case .setResponsiveness:
			responsiveness = level
		case .emitSignal0:
			let emitThreshold = 0.5
			let adjustedLevel = level * environment.responseCurve(responsiveness)
			if adjustedLevel > emitThreshold
			{
				environment.incrementSignal(layer: 0, location: self.location)
			}
		case .killForward:
			if level > Individual.killThreshold && EvoKit.rng.randomBool(in: Action.range, factor: level)
			{
				let otherLoc = location + lastMoveDir.normalisedVector
				_ = environment.visit(location: otherLoc, radius: 0)
				{
					coordinate, cell in
					assert(coordinate == otherLoc)
					if case Grid.Cell.individual(let individualIndex) = cell
					{
						environment.markForDeath(index: individualIndex)
					}
				}
			}
		case .moveX:
			movement.x += level
		case .moveY:
			movement.y += level
		case .moveFwd:
			movement += lastMoveDir.unitSIMD * level
		case .moveRandom:
			let randomDir = Direction.random()
			movement += randomDir.unitSIMD * level
		case .moveEast:
			movement.x += level
		case .moveWest:
			movement.x -= level
		case .moveNorth:
			movement.y += level
		case .moveSouth:
			movement.y -= level
		case .moveLeft:
			movement -= lastMoveDir.rotated(1).unitSIMD * level
		case .moveRight, .moveRL:
			movement += lastMoveDir.rotated(1).unitSIMD * level
		case .moveReverse:
			movement -= lastMoveDir.unitSIMD * level
		}
	}
}

/// A collection that is indexed by action
struct ActionLevels
{
	private var values: [Action : Double] = [:]

	subscript(position: Action) -> Double
	{
		get { values[position] ?? 0.0 }
		set { values[position] = newValue }
	}

	var keys: [Action] { Action.allCases }
	var count: Int { Action.allCases.count }
}
