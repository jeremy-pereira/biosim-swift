//
//  Genome.swift
//  
//
//  Created by Jeremy Pereira on 05/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox
import EvoKit

private let log = Logger.getLogger("biosim-swift.Genome").with(level: .info)

/// A sequence of genes for an individual
///
///
struct Genome
{
	private let genes: [Gene]

	init(dna: DNA)
	{
		var genes: [Gene] = []
		var byteIterator = dna.makeIterator()
		while let nextGene = Gene(iterator: &byteIterator)
		{
			genes.append(nextGene)
		}
		self.init(genes: genes)
	}

	init(genes: [Gene])
	{
		self.genes = genes
	}

	static let basesPerGene = Gene.basesNeeded

	/// Generate a child genome
	///
	/// This can either pik the parents completely at random, or with a bias
	/// towards the earlier elements of the array passed in. The array should
	/// therefore be ordered by score.
	/// - Parameters:
	///   - possibleParents: Parent DNA strands we can use
	/// - Returns: A child hopefully
	static func generateChildDNA(possibleParents: [DNA],
									chooseByFitness: Bool,
									sexual: Bool,
									pointMutationRate: Double) -> DNA
	{
		assert(!possibleParents.isEmpty)
		// Choose two parents
		let parent1: DNA
		let parent2: DNA
		if chooseByFitness && possibleParents.count > 1
		{
			let parent1Index = Int.random(in: 1 ..< possibleParents.endIndex, using: &EvoKit.rng)
			parent1 = possibleParents[parent1Index]
			parent2 = possibleParents[0 ..< parent1Index].randomElement(using: &EvoKit.rng)!
		}
		else
		{
			parent1 = possibleParents.randomElement(using: &EvoKit.rng)!
			parent2 = possibleParents.randomElement(using: &EvoKit.rng)!
		}
		var child: DNA
		if sexual
		{
			child = parent1.recombined(with: parent2, expectedCrossOvers: 2)
		}
		else
		{
			child = parent2 // This is the one selected with a bias if the score is used
		}
		child = child.randomInsertionDeletion().applyPointMutations(pointMutationRate: pointMutationRate)
		return child
	}
}

extension Genome: Collection
{
	var startIndex: Int { genes.startIndex }
	var endIndex: Int { genes.endIndex }
	subscript(position: Int) -> Gene { genes[position] }
	func index(after i: Int) -> Int { genes.index(after: i) }
}

extension Genome
{
	/// An individual gene
	///
	/// Each gene represents a connection between two neurons or between sensors
	/// and neurons, sensors and actions or neurons and actions.
	///
	/// A gene is actually two sixteen bit numbers, one of which tells us which
	/// two nodes are connected and the other tells us the weight of the
	/// connection.
	struct Gene
	{
		private var connection: ConnectionInfo
		private(set) var weight: Int16
		static let basesNeeded = MemoryLayout<Int16>.size + MemoryLayout<UInt16>.size

		private init(connection: ConnectionInfo, weight: Int16)
		{
			self.connection = connection
			self.weight = weight
		}

		init?<I: IteratorProtocol>(iterator: inout I) where I.Element == UInt8
		{
			guard let byte0 = iterator.next() else { return nil }
			guard let byte1 = iterator.next() else { return nil }
			guard let byte2 = iterator.next() else { return nil }
			guard let byte3 = iterator.next() else { return nil }
			let connectInfo = ConnectionInfo(rawValue: UInt16(byte0) | (UInt16(byte1) << 8))
			let weight = Int16(bitPattern: UInt16(byte2) | (UInt16(byte3) << 8))
			self.init(connection: connectInfo, weight: weight)
		}

		var sourceIsSensor: Bool
		{
			connection.contains(.sourceSensorNeuronMask)
		}

		var sourceIsNeuron: Bool { !sourceIsSensor }

		var sinkIsAction: Bool
		{
			connection.contains(.sinkActionNeuronMask)
		}

		var sinkIsNeuron: Bool { !sinkIsAction }

		var source: UInt16
		{
			get { connection.intersection(.sourceNumMask).rawValue >> ConnectionInfo.sourceLSB }
			set
			{
				connection.subtract(.sourceNumMask)
				connection.formUnion(ConnectionInfo(rawValue: newValue << ConnectionInfo.sourceLSB).intersection(.sourceNumMask))
			}
		}

		var sink: UInt16
		{
			get { self.connection.intersection(.sinkNumMask).rawValue >> ConnectionInfo.sinkLSB }
			set
			{
				self.connection.subtract(.sinkNumMask)
				self.connection.formUnion(ConnectionInfo(rawValue: newValue << ConnectionInfo.sinkLSB).intersection(.sinkNumMask))
			}
		}

		/// Returns the weight as a small value floating point
		///
		/// It looks like the range is `-2.0 ..< 2.0` judging by the constant
		/// chosen. Not sure if it should be `-1 ..< 1` or if it matters that much
		var weightAsDouble: Double { Double(weight) / 8192 }
	}
}

private extension Genome.Gene
{
	struct ConnectionInfo: OptionSet
	{
		var rawValue: UInt16

		static let sourceLSB = 1
		static let sinkLSB = (sourceLSB + 8)
		static let sourceSensorNeuronMask = ConnectionInfo(rawValue: 0b0000_0000_0000_0001)
		static let sourceNumMask          = ConnectionInfo(rawValue: 0b111_1111 << sourceLSB)
		static let sinkActionNeuronMask   = ConnectionInfo(rawValue: 0b0000_0001_0000_0000)
		static let sinkNumMask            = ConnectionInfo(rawValue: 0b111_1111 << sinkLSB)
	}
}
