//
//  SensorAction.swift
//  
//
//  Created by Jeremy Pereira on 29/11/2021.
//

/// Sensor types
enum Sensor: UInt16, CaseIterable
{
	/// Distance from left edge
	case locX = 0
	case locY
	case boundaryDistX
	case boundaryDist
	case boundaryDistY
	case geneticSimFwd
	case lastMoveDirX
	case lastMoveDirY
	case longProbePopFwd
	case longProbeBarFwd
	case population
	case populationFwd
	case populationLR
	case osc1
	case age
	case barrierFwd
	case barrierLR
	case random
	case signal0
	case signal0Fwd
	case signal0LR

	var name: String
	{
		switch self
		{

		case .locX:
			return "location X"
		case .locY:
			return "location Y"
		case .boundaryDistX:
			return "boundary distance X"
		case .boundaryDist:
			return "closest boundary distance"
		case .boundaryDistY:
			return "boundary distance Y"
		case .geneticSimFwd:
			return "genetic simularity fwd"
		case .lastMoveDirX:
			return "last move direction X"
		case .lastMoveDirY:
			return "last move direction X"
		case .longProbePopFwd:
			return "long probe population fwd"
		case .longProbeBarFwd:
			return "long probe barrier fwd"
		case .population:
			return "population"
		case .populationFwd:
			return "population fwd"
		case .populationLR:
			return "population left/right"
		case .osc1:
			return "oscilator 1"
		case .age:
			return "age"
		case .barrierFwd:
			return "short probe barrier fwd"
		case .barrierLR:
			return "short probe barrier left/right"
		case .random:
			return "random"
		case .signal0:
			return "signal 0"
		case .signal0Fwd:
			return "signal 0 fwd"
		case .signal0LR:
			return "signal 0 left/right"
		}
	}
}

enum Action: UInt16, CaseIterable
{
	case setResponsiveness = 0
	case moveX
	case moveY
	case moveFwd
	case moveRL
	case moveRandom
	case setOscilatorPeriod
	case setLongProbeDist
	case emitSignal0
	case moveEast
	case moveWest
	case moveNorth
	case moveSouth
	case moveLeft
	case moveRight
	case moveReverse
	case killForward

	var name: String
	{
		switch self
		{

		case .moveX:
			return "move X"
		case .moveY:
			return "move Y"
		case .moveFwd:
			return "move fwd"
		case .moveRL:
			return "move move right/left"
		case .moveRandom:
			return "move random"
		case .setOscilatorPeriod:
			return "set oscilator period"
		case .setLongProbeDist:
			return "set long probe distance"
		case .setResponsiveness:
			return "set responsiveness"
		case .emitSignal0:
			return "emit signal 0"
		case .moveEast:
			return "move east"
		case .moveWest:
			return "move west"
		case .moveNorth:
			return "move north"
		case .moveSouth:
			return "move south"
		case .moveLeft:
			return "move left"
		case .moveRight:
			return "move right"
		case .moveReverse:
			return "move reverse"
		case .killForward:
			return "move kill fwd"
		}
	}

	var isMovement: Bool
	{
		switch self
		{
		case .moveX,.moveY, .moveFwd, .moveRL, .moveRandom, .moveEast, .moveWest,
			 .moveNorth, .moveSouth, .moveLeft, .moveRight, .moveReverse:
			return true
		case .setOscilatorPeriod, .setLongProbeDist, .setResponsiveness,
			 .emitSignal0, .killForward:
			return false
		}
	}
	/// Range of values for action levels
	static let range = 0.0 ... 1.0

}

/// A protocol that describes the interface between the sensors and the
/// environment
protocol SensorEnvironment
{
	/// The population by index
	var peeps: Peeps { get }
	/// The radius over which an individual can see another individual
	var populationSensorRadius: Double { get }
	/// The number of steps in the simulation between generations
	///
	/// Used for determining age output
	var stepsPerGeneration: Int { get }

	/// The limit of a short range barrier probe
	var shortProbeBarrierDistance: Int { get }

	/// The radius around which we can detect signals
	var signalSensorRadius: Double { get }

	/// Determine the current location of the given individual
	/// - Returns: The location of the individual based on where the grid
	///            thinks it is.
	func location(of individual: Int) -> Coordinate?
	/// the size of the grid
	var gridSize: Vector { get }

	/// A visitor function to visit cells in a specific radius from a coordinate
	///
	/// Note that the order of calls to the action is not guaranteed
	/// - Parameters:
	///   - location: the centre of the region
	///   - radius: how far from the centre to look
	///   - action: What to do at each cell found. The coordinates and contents of the cell
	///             are passed in to the closure
	/// - Returns: thre number of locations visited
	/// - Throws: If the closure called throws
	func visit(location: Coordinate, radius: Double, _ action: (Coordinate, Grid.Cell) throws -> ()) rethrows -> Int

	/// Performs a reduction over the cells in the given radius from the location
	///
	/// This works like a normal reduce but on a region of cells in the
	/// grid. Note that the order of calls to the action is not guaranteed
	/// - Parameters:
	///   - location: the centre of the region
	///   - radius: how far from the centre to look
	///   - initial: tje initial value of T
	///   - action: What to do at each cell found. The coordinates and contents of the cell
	///             are passed in to the closure with an intermediate T. The closure
	///             must return the next value of T.
	/// - Returns: The final T value
	func reduce<T>(location: Coordinate, radius: Double, initial: T, _ action: (Coordinate, Grid.Cell, T) throws -> T) rethrows -> T

	/// Probe in a certain direction for cells containing a certain type of object
	///
	/// This probe can see past individuals but it can't see past barriers. Thus, if looking for a barrier, it
	/// will be seen regardless of any individuals in the way, but, if looking for an individual, it will
	/// not be seen if there is a barrier in the way.
	/// - Parameters:
	///   - location: where to start probing
	///   - direction: which direction to probe in
	///   - type: What kind of thing to look for (for individuals, the index is ignored)
	///   - distance: Maximum distance to probe
	/// - Returns: the distance to the first detected matching cell
	func longProbe(location: Coordinate, direction: Direction, type: Grid.Cell, distance: Int) -> Double

	/// Probe for the nearest thing in a given direction
	/// - Parameters:
	///   - location: the location to probe from
	///   - direction: the axis to probe on
	///   - type: What to look for. With individuals, the index is ignored
	///   - distance: maximum distance to probe
	/// - Returns: A number between 0 and 1. If the nearest thing is in front, a
	///            number > 0.5 is returned, if behind, a number between 0 and 0.5
	///            is returned. If no thing is found, 0.5 is returned.
	func shortProbe(location: Coordinate, direction: Direction, type: Grid.Cell, distance: Int) -> Double

	/// Find the population density along an axis from a point
	///
	///  The locations of neighbouts are scaled by the inverse of their duitance x the projection onto the
	///  direction vector.
	///  An empty neighbourhood gives a midrange value. Otherwise the value is skewed in the direction
	///  forwards or backwards of the highest density.
	/// - Parameters:
	///   - location: A point on the axis
	///   - direction: the direction of the axis
	/// - Returns: The population densisty between 0 and 1
	func populationDensity(location: Coordinate, direction: Direction) -> Double

	/// Get the signal density for a particular layer
	/// - Parameters:
	///   - layer: the layer to check
	///   - location: The centre of the neighbourhood to check
	/// - Returns: The signal density for the given layer
	func signalDensity(layer: Int, location: Coordinate) -> Double
	/// Get the signal density for a particular layer along an axis
	/// - Parameters:
	///   - layer: the layer to check
	///   - location: The centre of the neighbourhood to check
	///   - direction: The direction to check in
	/// - Returns: The signal density for the given layer
	func signalDensity(layer: Int, location: Coordinate, direction: Direction) -> Double
}

extension SensorEnvironment
{

	/// A default implementation for short problng that depends on long probing
	/// - Parameters:
	///   - location: The location to start from
	///   - direction: The forward direction
	///   - type: The type of cell to look for
	///   - distance: Maximum distance to look
	/// - Returns: The short probe value in the range `0 ... 1`
	func shortProbe(location: Coordinate, direction: Direction, type: Grid.Cell, distance: Int) -> Double
	{
		let forwardDistance = longProbe(location: location,
										direction: direction,
										type: type,
										distance: distance)
		let reverseDistance = longProbe(location: location,
										direction: direction.rotated(2),
										type: type,
										distance: distance)
		assert(forwardDistance <= Double(distance))
		assert(reverseDistance <= Double(distance))
		return ((forwardDistance - reverseDistance) + Double(distance) / 2) / Double(distance)
	}

	func populationDensity(location: Coordinate, direction: Direction) -> Double
	{
		// Calculate a proper unit vector
		let length = Double(direction.normalisedVector.length2).squareRoot()
		let unNormalisedVector = SIMD2(Double(direction.normalisedVector.x),
									   Double(direction.normalisedVector.y))
		let unitVector = unNormalisedVector / length
		let sum = reduce(location: location, radius: populationSensorRadius, initial: Double(0))
		{
			coordinate, cell, partialSum in
			let contribution: Double
			if coordinate != location
			{
				switch cell
				{
				case .empty, .barrier:
					contribution = 0
				case .individual(_):
					let offset = coordinate - location
					let realOffset = SIMD2<Double>(offset.value)
					let projection = (realOffset * unitVector).sum()
					contribution = projection / (realOffset * realOffset).sum()
				}
			}
			else
			{
				contribution = 0
			}
			return partialSum + contribution
		}
		let maxSumMag = 6 * Double(populationSensorRadius)
		assert((-maxSumMag ... maxSumMag).contains(sum))
		return (sum / maxSumMag + 1) / 2
	}
}

protocol ActionEnvironment
{
	/// The actions fenabled for this simulation
	///
	///  Actions will be executed in the order they appear in this array. It's important for some subsequent
	///  actions that  ``Action.setResponsiveness`` is  first.
	var enabledActions: [Action] { get }

	/// Calculates a responsiveness on a response curve
	///
	/// - Parameters:
	///     - initialResponsiveness
	/// - Returns: An adjusted responsiveness
	func responseCurve(_ initialResponsiveness: Double) -> Double

	/// Increment the signal in  given layer at a given location
	///  - Parameters:
	///      - layer: layer of signal to imcrement
	///      - location: which location on the grist to increment
	mutating func incrementSignal(layer: Int, location: Coordinate)

	/// A visitor function to visit cells in a specific radius from a coordinate
	///
	/// Note that the order of calls to the action is not guaranteed
	/// - Parameters:
	///   - location: the centre of the region
	///   - radius: how far from the centre to look
	///   - action: What to do at each cell found. The coordinates and contents of the cell
	///             are passed in to the closure
	/// - Returns: thre number of locations visited
	/// - Throws: If the closure called throws
	func visit(location: Coordinate, radius: Double, _ action: (Coordinate, Grid.Cell) throws -> ()) rethrows -> Int

	/// Mark an individual for death
	func markForDeath(index: Int)
}
