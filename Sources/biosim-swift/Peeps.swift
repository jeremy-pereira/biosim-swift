//
//  Peeps.swift
//  
//
//  Created by Jeremy Pereira on 30/11/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

/// Keep track of individuals in the population
///
/// Peeps allows spawning a live Indiv at a random or specific location
/// in the grid, moving ``Individual``s from one grid location to another, and
/// killing any `Individual`s.
///
/// The ``Peeps/cull`` function will remove dead members and replace their slots
/// in the  [population] with living members from the end of the container for
/// compacting the container.
///
/// Each `Individual` has an identifying index that is  stored in the `Grid` at
/// the location where the `Individual` resides, such that a `Grid` element
/// value `.individual(n)` refers to the `Individual` at index `n` in this
/// collection. .
struct Peeps: MutableCollection
{
	var startIndex: Int { individuals.startIndex }

	var endIndex: Int { individuals.endIndex }

	private var deathQueue: [Int] = []
	private var moveQueue: [(Int, Coordinate)] = []

	private var individuals: [Individual]

	init(count: Int)
	{
		individuals = Array(repeating: Individual(), count: count)
	}

	subscript(position: Int) -> Individual
	{
		get { individuals[position] }
		set { individuals[position] = newValue }
	}

	func index(after i: Int) -> Int { individuals.index(after: i) }

	mutating func queueForDeath(index: Int)
	{
		deathQueue.append(index)
	}

	mutating func queueForMove(index: Int, location: Coordinate)
	{
		moveQueue.append((index, location))
	}

	/// The  number of individuals in the death queue
	var deathQueueCount: Int { deathQueue.count }

	/// Kill off everything in the death queue
	/// - Parameter grid: The grid is needed to zap individuals who are dead
	mutating func drainDeathQueue(grid: inout Grid)
	{
		for index in deathQueue
		{
			assert(individuals[index].index == index)
			assert(grid[individuals[index].location] == Grid.Cell.individual(index))
			individuals[index].kill()
			grid[individuals[index].location] = .empty
		}
		deathQueue.removeAll()
	}

	mutating func drainMoveQueue(grid: inout Grid)
	{
		for (index, location) in  moveQueue
		{
			let oldLocation = individuals[index].location
			assert(grid[oldLocation] == .individual(index))
			if grid[location] == .empty
			{
				grid[location] = .individual(index)
				grid[oldLocation] = .empty
				individuals[index].move(to: location)
			}
		}
		moveQueue.removeAll()
	}
}
