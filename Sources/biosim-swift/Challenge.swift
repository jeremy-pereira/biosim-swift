//
//  Challenge.swift
//  
//
//  Created by Jeremy Pereira on 25/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

/// A protocol for desxcribing challenges
///
/// A challenge tells us how we score the survivors.
protocol Scoring
{

	/// A function that scores individuals
	/// - Parameters:
	///   - individual the individual to score
	/// - Returns: A bool for alive or not and a score
	func score(individual: Individual) -> (Bool, Double)

	/// If a scoring method needs persistant state, usse this to reset it
	func reset()

	/// True if this is an altruistic challenge
	var isAltruistic: Bool { get }

	/// Processing done for the challenge at the end of a sim step
	///
	/// - TODO: This is just a placeholder. It will need some parameters.
	func endSimStep()
}

extension Scoring
{
	/// Test if an individual passes a challenge
	///
	/// - Parameters:
	///    - individual: the individual to score
	/// - Returns: A tuple containing a `Bool` survival and a score.
	func passedSurvivalCriteria(individual: Individual) -> (Bool, Double)
	{
		guard individual.alive  else { return (false, 0) }
		return score(individual: individual)
	}
}


/// Provides a namespace for challenges
///
/// - TODO: Write all the challenges
enum Challenge
{
// TODO: write all the challenges


	/// Use the param manager to create a challenge.
	/// - Parameter params: The simulation parameters
	/// - Returns: A scoring method for the current simulation
	/// - Throws: if the parameters are insufficient to make a challeng or
	///           wrong.
	static func makeChallenge(params: ParamManager) throws -> Scoring
	{
		let challengeKey: String = try params.challenge()
		// The official challenges can be recongnised by number to keep
		//
		switch challengeKey
		{
		case "0", "circle":
			let sizeX: Int = try params.sizeX()
			let sizeY: Int = try params.sizeY()
			return Circle(rect: Rect(bottomLeft: Coordinate.origin, size: Vector(sizeX, sizeY)))
			/*
		case "1", "rightHalf":
			return RightHalf()
		case "2", "rightQuarter":
			return RightQuarter()
		case "3", "string":
			return StringScore
		case "4", "centreWeighted", "centerWeighted":
			return CentreWeighted()
		case "40", "centreUnweighted", "centerUnweighted":
			return CentreUnweighted()
		case "5", "corner":
			return Corner()*/
		case "6", "cornerWeighted":
			let sizeX: Int = try params.sizeX()
			let sizeY: Int = try params.sizeY()
			guard sizeX == sizeY else { throw Simulation.Error.gridMustBeSquare(sizeX, sizeY) }
			return CornerWeighted(rect: Rect(bottomLeft: Coordinate.origin, size: Vector(sizeX, sizeY)))
/*		case "7", "migrateDistance":
			return MigrateDistance()
		case "8", "centreSparse", "centerSparse":
			return CentreSparse()
		case "9", "leftEighth":
			return LeftEighth()
		case "10", "radioactiveWalls":
			return RadioactiveWalls()
		case "11", "againstAnyWall":
			return AgainstAnyWall()
		case "12", "touchAnyWall":
			return TouchAnyWall()
		case "13", "eastWestEighths":
			return EastWestEighths()
		case "14", "nearBarrier":
			return NearBarrier()
		case "15", "pairs":
			return Pairs()
		case "16", "locationSequence":
			return LocationSequence()
		case "17", "altruism":
			return Altruism()*/
		default:
			throw Simulation.Error.unknownChallenge(challengeKey)
		}
	}

	/// Survivors are those inside the circle in the bottom left hand quadrant
	/// of the grid
	struct Circle: Scoring
	{
		let safeCentre: Coordinate
		let radius: Double
		let isAltruistic = false

		init(rect: Rect)
		{
			safeCentre = rect.origin + rect.size / 4
			radius = Double(rect.size.x) / 4
		}

		func score(individual: Individual) -> (Bool, Double)
		{
			let offset = individual.location - safeCentre
			// Haven't got a length property that produces a Double directly
			let distance = Double(offset.length2).squareRoot()
			return distance <= radius ? (true, (radius - distance) / radius) : (false, 0)
		}

		func reset() {}
		func endSimStep() {}
	}

	struct CornerWeighted: Scoring
	{
		let corners: [Coordinate]
		let radius: Double
		let isAltruistic = false

		init(rect: Rect)
		{
			assert(rect.size.x == rect.size.y)
			corners = [
				rect.origin,
				rect.topRight - Vector.i - Vector.j, // Ensures the corner is in the Rect
				rect.topLeft - Vector.j,
				rect.bottomRight - Vector.i,
			]
			radius = Double(rect.size.x) / 4
		}

		func score(individual: Individual) -> (Bool, Double)
		{
			for corner in corners
			{
				let distance = Double((individual.location - corner).length2).squareRoot()
				if distance <= radius
				{
					return (true, (radius - distance) / radius)
				}
			}
			return (false, 0)
		}

		func reset() {}
		func endSimStep() {}
	}
}
