//
//  Simulation.swift
//  
//
//  Created by Jeremy Pereira on 28/11/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Toolbox
import Darwin
import EvoKit

private let log = Logger.getLogger("biosim-swift.Simulation")

/// Encapsulates an entire simulation
public class Simulation
{
	private enum RunMode
	{
		case stop
		case run
		case pause
		case abort
	}
	/// Simulation parameters
	private(set) var p: Parameters

	/// The world on which the simulation plays out
	public private(set) var grid: Grid
	var signals: Signals
	var peeps: Peeps
	private var runMode = RunMode.stop
	var challenge: Scoring

	private(set) var enabledActions: [Action]

	private var imageWriter: ImageWriter?

	public init(parameters: ParamManager) throws
	{
		self.p = try Parameters(paramManager: parameters)
		self.grid = try Grid(sizeX: parameters.sizeX(), sizeY: parameters.sizeY())
		self.signals = try Signals(layers: parameters.signalLayers(), sizeX: parameters.sizeX(), sizeY: parameters.sizeY())
		self.peeps = try Peeps(count: parameters.population())
		// It's important that `setResponsiveNess` is the first action, since it
		// affects a lot of the others.
		assert(Action.allCases.first! == .setResponsiveness)
		enabledActions = []
		for action in Action.allCases
		{
			let actionName = "\(action)"
			let key = "enable" + actionName.first!.uppercased() + actionName.dropFirst()
			if try parameters.getBool(key: key, defaultIfMissing: true)
			{
				enabledActions.append(action)
			}
		}
		challenge = try Challenge.makeChallenge(params: parameters)
		if p.saveVideo
		{
			let directoryPath: String = try parameters.imageDir()
			let imageDirectory = URL(fileURLWithPath: directoryPath)
			imageWriter = try ImageWriter(delegate: CIMGDelegate(displayScale: parameters.displayScale(),
																 imageDir: imageDirectory,
																 agentSize: parameters.agentSize()))
		}
	}

	/// Run the simulation
	///
	///  Parameters for the simulation are taken from ``parameters``
	public func run() throws
	{
		printSensorActions()
		var generation = 0
		try initialiseGeneration0()
		runMode = .run

		while runMode == .run && generation < p.maxGenerations
		{
			var murderCount = 0
			for simStep in 0 ..< p.stepsPerGeneration
			{
				for index in peeps.startIndex ..< peeps.endIndex
				{
					var individual = peeps[index]
					if individual.alive
					{
						simStepOne(individual: &individual, simStep: simStep)
					}
					peeps[index] = individual
				}
				murderCount += peeps.deathQueueCount
				log.debug("Murder count is \(murderCount)")
				end(simStep: simStep, generation: generation)
			}

			end(generation: generation)

			let numberOfSurvivors = try spawnNewGeneration(generation, murderCount: murderCount)
			if numberOfSurvivors > 0 && p.genomeAnalysisStride > 0 && generation % p.genomeAnalysisStride == 0
			{
				displaySampleGenomes(count: p.sampleGenomes)
			}

			if numberOfSurvivors == 0
			{
				runMode = .stop
			}
			else
			{
				generation += 1
			}
		}
		displaySampleGenomes(count: 3)
		print("Simulator exit")
	}

	private func simStepOne(individual: inout Individual, simStep: Int)
	{
		individual.age += 1
		let actionLevels = individual.feedForward(step: simStep, environment: self)
		execute(actions: actionLevels, individual: &individual)
	}

	/// Create a new population
	///
	///
	/// This will erase the grid and signal layers, then create a new population in
	/// the peeps container at random locations with random genomes.
	private func initialiseGeneration0() throws
	{
		grid.empty()
		grid.createBarrier(type: p.replaceBarrierTypeGenerationNumber == 0 ? p.replaceBarrierType : p.barrierType)
		signals.empty()
		let minDNALength: Int = p.genomeInitialLengthMin * Genome.basesPerGene
		let maxDNALength: Int = p.genomeInitialLengthMax * Genome.basesPerGene
		for index in 0 ..< p.population
		{
			guard let location = grid.findEmptyLocation()
			else { throw Simulation.Error.gridIsFull }
			peeps[index] = Individual(index: index,
								 	  location: location,
									  dna: DNA.makeRandom(minLength: minDNALength,
														  maxLength: maxDNALength),
									  longProbeDistance: p.longProbeDistance,
									  maxNumberNeurons: p.maxNumberNeurons)
			{
				grid[$0.location] = .individual($0.index)
			}
		}
	}

	private func initialiseNewGeneration(parentDNA: [DNA], generation: Int) throws
	{
		grid.empty()
		grid.createBarrier(type: generation >= p.replaceBarrierTypeGenerationNumber ? p.replaceBarrierType : p.barrierType)
		signals.empty()
		let populationSize: Int = p.population
		let chooseByFitness: Bool = p.chooseParentsByFitness
		let sexualReproduction: Bool = p.sexualReproduction
		let pointMutationRate: Double =  p.pointMutationRate
		for index in 0 ..< populationSize
		{
			guard let location = grid.findEmptyLocation()
			else { throw Simulation.Error.gridIsFull }
			peeps[index] = Individual(index: index,
									  location: location,
									  dna: Genome.generateChildDNA(possibleParents: parentDNA,
																   chooseByFitness: chooseByFitness,
																   sexual: sexualReproduction,
																   pointMutationRate: pointMutationRate),
									  longProbeDistance: p.longProbeDistance,
									  maxNumberNeurons: p.maxNumberNeurons)
			{
				grid[$0.location] = .individual($0.index)
			}
		}
	}

	/// Spawn a new generation
	/// - Parameters:
	///   - generation: Current generation number
	///   - murderCount: Count of murdered individuals
	/// - Returns: The count of survivors
	private func spawnNewGeneration(_ generation: Int, murderCount: Int) throws -> Int
	{
		//var sacrificedCount = 0 // For the altruism challenge

		// Holds the indexes and survival scores of the survivors
		var parents: [(index: Int, score: Double)] = []

		challenge.reset()

		if !challenge.isAltruistic
		{
			for individual in peeps
			{
				let (passed, score) = challenge.passedSurvivalCriteria(individual: individual)
				if passed && !individual.neuralNet.connections.isEmpty
				{
					parents.append((individual.index, score))
				}
			}
		}
		else
		{
			notImplemented("Altruism challenges not accepted yet")
		}

		parents.sort{ $0.score > $1.score }
		let parentDNA: [DNA] = parents.map
		{
			let (index, _) = $0
			return peeps[index].dna
		}
		log.info("Gen \(generation), \(parentDNA.count) survivor(s)")
		try appendEpochLog(generation: generation,
						   numberSurvivors: parentDNA.count,
						   murderCount: murderCount)

		if !parentDNA.isEmpty
		{
			try initialiseNewGeneration(parentDNA: parentDNA, generation: generation + 1)
		}
		else
		{
			try initialiseGeneration0()
		}
		return parentDNA.count
	}

	private func execute(actions: ActionLevels, individual: inout Individual)
	{
		var movement = SIMD2<Double>.zero
		for action in self.enabledActions
		{
			// For movement actions we normalise the level after adding them all
			// up. For others, we prenormalise to 0 ... 1
			let level = action.isMovement ? actions[action] : (tanh(actions[action]) + 1) / 2
			var environment: ActionEnvironment = self
			individual.execute(action: action,
							   level: level,
							   movement: &movement,
							   environment: &environment)
		}
		// Now work out how to move the individual
		let responsivenessAdjusted = responseCurve(individual.responsiveness)
		// Normalise the movment vector
		movement = SIMD2(tanh(movement.x), tanh(movement.y)) * responsivenessAdjusted
		let willMoveX = EvoKit.rng.randomBool(factor: abs(movement.x))
		let willMoveY = EvoKit.rng.randomBool(factor: abs(movement.y))
		let xComponent = movement.x < 0 ? -1 : 1
		let yComponent = movement.y < 0 ? -1 : 1
		let components = Vector(willMoveX ? xComponent : 0, willMoveY ? yComponent : 0)
		let newLoc = individual.location + components
		if grid.rect.contains(newLoc) && grid[newLoc] == .empty
		{
			peeps.queueForMove(index: individual.index, location: newLoc)
		}
	}

	private func shouldSaveVideo(generation: Int) -> Bool
	{
		// If we have hit video stride, return true, no matter what
		if generation % p.videoStride == 0 { return true }

		// Otherwise, if we need to save the first few generations, we need
		// more checks
		guard p.videoSaveFirstFrames > 0 else { return false }

		// Return true if we are near the beginning
		if generation <= p.videoSaveFirstFrames { return true }
		// Also save the first few after a barrier change, if there is one
		guard p.replaceBarrierTypeGenerationNumber >= 0 else { return false }
		let replaceEnd = p.replaceBarrierTypeGenerationNumber + p.videoSaveFirstFrames
		return (p.replaceBarrierTypeGenerationNumber ... replaceEnd).contains(generation)
	}

	func end(simStep: Int, generation: Int)
	{
		log.debug("Ending simstep \(simStep), generation \(generation)")

		challenge.endSimStep() // TODO: will need to add some parameters to this
		peeps.drainDeathQueue(grid: &grid)
		peeps.drainMoveQueue(grid: &grid)
		signals.fade()

		if var imageWriter = imageWriter, shouldSaveVideo(generation: generation)
		{
			imageWriter.saveVideoFrameSync(simStep: simStep, generation: generation, individuals: peeps, signals: signals, grid: grid)
			self.imageWriter = imageWriter
		}
	}

	/// Stuff to do at the end of each generation
	/// - Parameter generation: Which generation just ended
	func end(generation: Int)
	{
		if var imageWriter = imageWriter, shouldSaveVideo(generation: generation)
		{
			imageWriter.saveVideo(generation: generation)
			self.imageWriter = imageWriter
		}

		if !endGenerationTodo
		{
			log.warn("TODO: end(generation:) should update graph log")
			endGenerationTodo = true
		}
		log.debug("Ending generation \(generation)")
	}

}

public extension Simulation
{
	/// Errors that can be thrown during a simulation run
	enum Error: Swift.Error
	{
		case parameterConversionFailed(String, destType: String, value: String)
		case missingParameter(String)
		case gridIsFull
		case unknownChallenge(String)
		case gridMustBeSquare(Int, Int)
		case gridOrDisplayScaleTooLarge(dimension: String, size: Int, scale: Int)
		case cannotCreateDirectory(URL)
	}
}

private var endSimStepTodo = false
private var endGenerationTodo = false

// MARK: Display stuff
private extension Simulation
{

	func displaySampleGenomes(count: Int)
	{
		log.warn("TODO: display \(count) genome(s)")
	}

	func printSensorActions()
	{
		// TODO: Don't just print to stdout
		print("Sensors:")
		Sensor.allCases.forEach
		{
			print("    " + $0.name)
		}
		print("Actions:")
		Action.allCases.forEach
		{
			print("    " + $0.name)
		}
	}

	func appendEpochLog(generation: Int, numberSurvivors: Int, murderCount: Int) throws
	{
		log.warn("TODO: Need an epoch log")
	}
}

extension Simulation: SensorEnvironment
{
	var populationSensorRadius: Double { p.populationSensorRadius }

	var stepsPerGeneration: Int { p.stepsPerGeneration }

	var shortProbeBarrierDistance: Int { p.shortProbeBarrierDistance }

	var signalSensorRadius: Double { p.signalSensorRadius }

	func location(of individual: Int) -> Coordinate?
	{
		guard (0 ..< peeps.count).contains(individual)
		else { return nil }
		let location = peeps[individual].location
		let referredCell = grid[location]
		guard referredCell == Grid.Cell.individual(individual)
		else { fatalError("Location inconsistency between peeps and grid") }
		return location
	}

	var gridSize: Vector { grid.size }

	func visit(location: Coordinate, radius: Double, _ action: (Coordinate, Grid.Cell) throws -> ()) rethrows -> Int
	{
		return try grid.reduce(location: location, radius: radius, initial: 0)
		{
			try action($0, $1)
			return $2 + 1
		}
	}

	func reduce<T>(location: Coordinate,
				   radius: Double,
				   initial: T,
				   _ action: (Coordinate, Grid.Cell, T) throws -> T) rethrows -> T
	{
		return try grid.reduce(location: location, radius: radius, initial: initial, action)
	}

	func longProbe(location: Coordinate, direction: Direction, type: Grid.Cell, distance: Int) -> Double
	{
		assert(direction != Direction.centre)

		let distance2 = distance * distance
		var currentLoc = location + direction.normalisedVector
		var blocked = false
		var found: Coordinate? = nil
		while grid.rect.contains(currentLoc)
				&& (currentLoc - location).length2 <= distance2
				&& !blocked
				&& found == nil
		{
			let cell = grid[currentLoc]
			switch cell
			{
			case .empty:
				if (type == .empty)
				{
					found = currentLoc
				}
			case .barrier:
				blocked = true
				if type == .barrier
				{
					found = currentLoc
				}
			case .individual(_):
				if case .individual(_) = type
				{
					found = currentLoc
				}
			}
			currentLoc += direction.normalisedVector
		}
		if let found = found
		{
			let foundDist2 = (found - location).length2
			return Double(foundDist2).squareRoot()
		}
		else
		{
			return Double(distance)
		}
	}

	func signalDensity(layer: Int, location: Coordinate) -> Double
	{
		let layer = signals.layers[layer]
		let (count, sum) = layer.reduce(location: location, radius: signalSensorRadius, initial: (0, 0))
		{
			coordinate, value, partial in
			let (count, sum) = partial
			return (count + 1, sum + Int(value))
		}
		let maxSum = Double(count) * Double(Signals.maxStrength)
		return Double(sum) / maxSum
	}

	func signalDensity(layer: Int, location: Coordinate, direction: Direction) -> Double
	{
		assert(direction != Direction.centre)

		let layer = signals.layers[layer]

		let length = Double(direction.normalisedVector.length2).squareRoot()
		let unNormalisedVector = SIMD2(Double(direction.normalisedVector.x),
									   Double(direction.normalisedVector.y))
		let unitVector = unNormalisedVector / length
		let sum = layer.reduce(location: location, radius: signalSensorRadius, initial: Double(0))
		{
			coordinate, magnitude, partialSum in
			let contribution: Double
			if coordinate != location
			{
				let offset = coordinate - location
				let realOffset = SIMD2<Double>(offset.value)
				let projection = (realOffset * unitVector).sum()
				contribution = projection * Double(magnitude) / (realOffset * realOffset).sum()
			}
			else
			{
				contribution = 0
			}
			return partialSum + contribution
		}
		let maxSumMag = 6 * Double(signalSensorRadius) * Double(Signals.maxStrength)
		assert((-maxSumMag ... maxSumMag).contains(sum))
		return (sum / maxSumMag + 1) / 2
	}
}

extension Simulation: ActionEnvironment
{
	func markForDeath(index: Int)
	{
		// Hopefully the indicvidual thinks it is in the same place as the grid
		// thinks.
		assert(Grid.Cell.individual(index) == grid[peeps[index].location])

		if peeps[index].alive
		{
			peeps.queueForDeath(index: index)
		}
	}

	/// Adjust responsiveness on an exponential curve
	///
	/// This takes a probability from 0.0..1.0 and adjusts it according to an
	/// exponential curve. The steepness of the curve is determined by the K
	/// factor which is a small positive integer. This tends to reduce the
	/// activity level a bit making individuals less jittery.
	///
	/// - Parameter initialResponsiveness: Responiveness to adjust
	/// - Returns: An adjusted responsiveness
	func responseCurve(_ initialResponsiveness: Double) -> Double
	{
		assert((0.0 ... 1.0).contains(initialResponsiveness))
		let r = initialResponsiveness
		let k = p.responsivenessCurveFactor
		return pow(r - 2, -2 * k) - pow(2, -2 * k) * (1 - r)
	}

	func incrementSignal(layer: Int, location: Coordinate)
	{
		signals.increment(layer: 0, location: location)
	}
}

extension Simulation
{
	struct Parameters
	{
		// MARK: General
		let maxGenerations: Int
		let stepsPerGeneration: Int
		let genomeAnalysisStride: Int
		let sampleGenomes: Int
		let replaceBarrierTypeGenerationNumber: Int
		let replaceBarrierType: Grid.BarrierType
		let barrierType: Grid.BarrierType
		let population: Int
		let responsivenessCurveFactor: Double
		// MARK: Genome
		let genomeInitialLengthMin: Int
		let genomeInitialLengthMax: Int
		// MARK: Individual
		let longProbeDistance: Int
		let maxNumberNeurons: Int
		// MARK: selection and reproduction
		let chooseParentsByFitness: Bool
		let sexualReproduction: Bool
		let pointMutationRate: Double
		// MARK: Sensor stuff
		let populationSensorRadius: Double
		let shortProbeBarrierDistance: Int
		let signalSensorRadius: Double
		// MARK: Video
		let saveVideo: Bool
		let videoSaveFirstFrames: Int
		let videoStride: Int

		init(paramManager: ParamManager) throws
		{
			self.maxGenerations = try paramManager.maxGenerations()
			self.stepsPerGeneration = try paramManager.stepsPerGeneration()
			self.genomeAnalysisStride = try paramManager.genomeAnalysisStride()
			self.sampleGenomes = try paramManager.displaySampleGenomes()
			self.replaceBarrierTypeGenerationNumber = try paramManager.replaceBarrierTypeGenerationNumber()
			self.replaceBarrierType = try paramManager.replaceBarrierType()
			self.barrierType = try paramManager.barrierType()
			self.population = try paramManager.population()
			self.responsivenessCurveFactor = try paramManager.responsivenessCurveFactor()
			self.genomeInitialLengthMin = try paramManager.genomeInitialLengthMin()
			self.genomeInitialLengthMax = try paramManager.genomeInitialLengthMax()
			self.longProbeDistance = try paramManager.longProbeDistance()
			self.maxNumberNeurons = try paramManager.maxNumberNeurons()
			self.chooseParentsByFitness = try paramManager.chooseParentsByFitness()
			self.sexualReproduction = try paramManager.sexualReproduction()
			self.pointMutationRate = try paramManager.pointMutationRate()
			self.populationSensorRadius = try paramManager.populationSensorRadius()
			self.shortProbeBarrierDistance = try paramManager.shortProbeBarrierDistance()
			self.signalSensorRadius = try paramManager.signalSensorRadius()
			self.saveVideo = try paramManager.saveVideo()
			self.videoSaveFirstFrames = try paramManager.videoSaveFirstFrames()
			self.videoStride = try paramManager.videoStride()
		}
	}
}
