//
//  Signals.swift
//  
//
//  Created by Jeremy Pereira on 30/11/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


/// Describes any pheremone signals that might be present in a grid
public struct Signals
{
	/// The maximum strength of a signal
	static let maxStrength = UInt8.max
	private static let centreIncreaseAmount: UInt8 = 2
	private static let neighbourIncreaseAmount: UInt8 = 1

	/// There are multiple layers to allow for multiple pheremones.
	public private(set) var layers: [Layer]
	init(layers: Int, sizeX: Int, sizeY: Int)
	{
		self.layers = Array<Layer>(repeating: Layer(sizeX: sizeX, sizeY: sizeY), count: layers)
	}

	mutating func empty()
	{
		layers.mutateEach
		{
			$0.empty()
		}
	}

	/// Fade all the layers of signals
	mutating func fade()
	{
		layers.mutateEach { $0.fade() }
	}

	/// Increment the signal value for the given layer at a coordinate.
	/// - Parameters:
	///   - layer: The layer to increment
	///   - location: The location that provides the increment centre
	mutating func increment(layer: Int, location: Coordinate)
	{
		_ = self.layers[layer].visit(location: location, radius: 1.5)
		{
			coordinate, value in

			let incrementAmout = coordinate == location ? Signals.centreIncreaseAmount
			                                            : Signals.neighbourIncreaseAmount
			if value < Signals.maxStrength - incrementAmout
			{
				value += incrementAmout
			}
			else
			{
				value = Signals.maxStrength
			}
		}
	}
}

extension Signals
{
	public struct Layer
	{
		private var data: [[UInt8]]
		private let size: Vector

		fileprivate init(sizeX: Int, sizeY: Int)
		{
			data = Array<Array<UInt8>>(repeating: Array<UInt8>(repeating: 0, count: sizeY), count: sizeX)
			size = Vector(sizeX, sizeY)
		}

		mutating func empty()
	 	{
			data.mutateEach
		 	{
			 	$0.mutateEach
			 	{
				 	$0 = 0
			 	}
		 	}
	 	}

		mutating func fade()
		{
			let rect = Rect(bottomLeft: Coordinate.origin, size: size)
			for coordinate in rect
			{
				let oldValue = self[coordinate]
				self[coordinate] = oldValue > 0 ? (oldValue - 1) : 0
			}
		}
	}
}

extension Signals.Layer: MutableAreaVisitable
{
	func clip(_ rect: Rect) -> Rect
	{
		let newBottomLeft = Coordinate(max(0, rect.origin.x), max(0, rect.origin.y))
		let newTopRight = Coordinate(min(size.x, rect.topRight.x), min(size.y, rect.topRight.y))
		return Rect(bottomLeft: newBottomLeft, size: newTopRight - newBottomLeft)
	}

	subscript(coordinate: Coordinate) -> UInt8
	{
		get { return data[coordinate.x][coordinate.y] }
		set { data[coordinate.x][coordinate.y] = newValue }
	}
}
