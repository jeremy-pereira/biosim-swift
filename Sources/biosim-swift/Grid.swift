//
//  Grid.swift
//  
//
//  Created by Jeremy Pereira on 30/11/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox
import EvoKit

/// The world in which the simulation takes place
///
/// This is a two dimensional array of cells which can either be empty, a barrier
/// or the index of an `Individual` in `Simulation/peeps`
public struct Grid
{
	/// Locations of all the barriers
	public private(set) var barrierLocations: [Coordinate] = []
	/// Locations of the centres of the barriers
	///
	/// This is not used for all barrier types
	public private(set) var barrierCentres: [Coordinate] = []

	/// The content of a cell in the grid
	public enum Cell: Equatable, CustomStringConvertible
	{
		/// The cell is empty
		case empty
		/// The cell is a barrier
		case barrier
		/// The cell contains an individual indexed by the associated value
		case individual(Int)

		/// A string description of what is in the cell
		///
		/// If the cell is empty, this returns `"."`. If it contains a barrier,
		/// it returns `"B"`. If it contains an individual, it returns the last
		/// digit of its index (in base 10).
		public var description: String
		{
			switch self
			{
			case .empty:
				return "."
			case .barrier:
				return "B"
			case .individual(let index):
				return "\(index % 10)"
			}
		}
	}

	private var cells: [[Cell]]
	/// The size of this grid as a vector
	public let size: Vector

	public var rect: Rect { Rect(bottomLeft: Coordinate.origin, size: size) }

	init(sizeX: Int, sizeY: Int)
	{
		cells = Array<Array<Cell>>(repeating: Array<Cell>(repeating: .empty, count: sizeY), count: sizeX)
		self.size = Vector(sizeX, sizeY)
	}

	mutating func empty()
	{
		cells.mutateEach
		{
			$0.mutateEach
			{
				$0 = .empty
			}
		}
	}

	/// Fills the given rectangle with the given cell
	///
	/// The rectangle is clipped by the grid, so filling a rectangle with cells
	/// not in the grid doesn't fail.
	/// - Parameters:
	///   - rect: The rectangle to fill
	///   - cell: The cell type with which to fill
	mutating func fill(rect: Rect, with cell: Cell)
	{
		let clippedRect = self.clip(rect)
		for coordinate in clippedRect
		{
			self[coordinate] = cell
		}
	}

	/// Create barriers for the grid
	///
	/// This function should only be called whern the grid is empty. Otherwise
	/// it stands a chance of zapping some of the individuals in it.
	/// - Parameter type: The type of barrier to create
	mutating func createBarrier(type: BarrierType)
	{
		barrierLocations.removeAll()
		barrierCentres.removeAll()
		type.draw(onGrid: &self)
	}

	/// Gets the cells of the grid in row major order
	///
	/// This is a convenience function for printing. As it iterates throgh
	/// all the cells in the grid, it is `O(x * y)`
	public var cellsInRowMajorOrder: [[Cell]]
	{
		var rows: [[Cell]] = []

		for y in 0 ..< size.y
		{
			var row: [Cell] = []
			for x in 0 ..< size.x
			{
				row.append(cells[x][y])
			}
			rows.append(row)
		}
		return rows
	}
	/// Subscript using x and y coordinates
	///
	/// - Parameters:
	///   - x: the x coordinate
	///   - y: the y coordinate
	public subscript(x: Int, y: Int) -> Cell
	{
		get { cells[x][y] }
		set { cells[x][y] = newValue }
	}
	/// Subscript using a coordinate
	///
	/// - Parameters:
	///   - coordinate: the coordinate of the cell
	public subscript(coordinate: Coordinate) -> Cell
	{
		get { cells[coordinate.x][coordinate.y] }
		set { cells[coordinate.x][coordinate.y] = newValue }
	}

	/// Find a random empty location in the grid
	/// - Parameter rect: The rect in the grid in which to look or `nil` if
	///                   you want the whole grid to be searched. Defaults to
	///                   `nil`.
	/// - Returns: A random coordinate that is empty
	func findEmptyLocation(in rect: Rect? = nil) -> Coordinate?
	{
		let findRect = self.clip(rect ?? self.rect)

		// Look at random locations. If we do not find one within 1000 attempts
		// use a more systematic approach
		var potentialLoc = findRect.randomElement(using: &EvoKit.rng)!
		var attemptCount = 0
		while self[potentialLoc] != .empty && attemptCount < 1000
		{
			attemptCount += 1
			potentialLoc = findRect.randomElement(using: &EvoKit.rng)!
		}
		guard self[potentialLoc] == .empty else { return potentialLoc }

		// We didn't find an empty location in 1000 random attempts. We'll use a
		// more systematic approach
		let emptyLocations = findRect.filter{ self[$0] == .empty }
		return emptyLocations.randomElement(using: &EvoKit.rng)
	}

	/// Clips the given rect to have locations only in the grid
	///
	/// If the `Rect` is completely outside the grid, a `Rect` with the same
	/// bottom left but zero size is returned.
	/// - Parameter rect: The ``Rect`` to clip.
	/// - Returns: `rect` clipped to the grid.
	func clip(_ rect: Rect) -> Rect
	{
		let newBottomLeft = Coordinate(max(0, rect.origin.x), max(0, rect.origin.y))
		let newTopRight = Coordinate(min(size.x, rect.topRight.x), min(size.y, rect.topRight.y))
		return Rect(bottomLeft: newBottomLeft, size: newTopRight - newBottomLeft)
	}
}

extension Grid
{
	// MARK: BarrierType
	/// Different types of barrier
	///
	/// Actually, this is an enum that dictates the pattern of any barrier we
	/// might want. New parrier arrangements have to be programmed at this point.
	///
	/// - Todo: devise a way to configure arbitrary barriers
	public enum BarrierType: Int
	{
		/// No barriers at all
		case none = 0
		/// A vertical barrier in the middle
		///
		/// This barrier is horizontally and vertically centered. It's one or
		/// two cells wide, depending on whether the width is odd or even and
		/// it's half the height tall roughly dependign on rounding errors that
		/// I can't be bothered to fix
		case verticalBarConstantLocation = 1
		/// TODO: implement
		case verticalBarRandomLocations = 2
		/// TODO: implement
		case fiveStaggerdVerticalBars = 3
		/// TODO: implement
		case horizBarConstantLocationNorthCentre = 4
		/// TODO: implement
		case floatingIslands = 5
		/// TODO: implement
		case sequenceOfSpots = 6

		func draw(onGrid grid: inout Grid)
		{
			switch self
			{
			case .none:
				break
			case .verticalBarConstantLocation:
				let bottomLeft = Coordinate(grid.size.x / 2 - (grid.size.x.isMultiple(of: 2) ? 1 : 0),
											grid.size.y / 4)
				let size = Vector(grid.size.x.isMultiple(of: 2) ? 2 : 1, grid.size.y / 2)
				let barrierRect = Rect(bottomLeft: bottomLeft, size: size)
				grid.fill(rect: barrierRect, with: .barrier)
				grid.barrierLocations = Array(barrierRect)
			case .verticalBarRandomLocations:
				break
			case .fiveStaggerdVerticalBars:
				break
			case .horizBarConstantLocationNorthCentre:
				break
			case .floatingIslands:
				break
			case .sequenceOfSpots:
				break
			}
		}
	}
}

extension Grid: AreaReducible {}

extension Grid: CustomStringConvertible
{

	/// The description is a rectangular array of the cells
	/// The bottom row is the row for x = 0
	public var description: String
	{
		let rows = cellsInRowMajorOrder
		let rowsAsString = rows.reversed().map
		{
			rowCells in
			rowCells.map { $0.description }.joined()
		}.joined(separator: "\n")
		return rowsAsString
	}
}
