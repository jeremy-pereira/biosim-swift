//
//  CIMGDelegate.swift
//  
//
//  Created by Jeremy Pereira on 03/01/2022.
//
//  Copyright (c) Jeremy Pereira xxxx
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import cimg_shim
import Foundation
import Toolbox

private let log = Logger.getLogger("biosim-swift.CIMGDelegate")

/// Used for creating videos with CImg
public struct CIMGDelegate: ImageWriterDelegate
{

	private let displayScale: Vector
	private let imageDir: URL
	private let agentSize: Int
	private var imageList = ImageList()

	/// Initialise a CIMgDelegate
	/// - Parameters:
	///   - displayScale: How many pixels per cell in the grid
	public init(displayScale: Int, imageDir: URL, agentSize: Int) throws
	{
		self.displayScale = Vector(displayScale, displayScale)
		self.imageDir = imageDir
		self.agentSize = agentSize
		var isDirectory: ObjCBool = false
		let directoryExists = FileManager.default.fileExists(atPath: imageDir.path, isDirectory: &isDirectory)
		switch (directoryExists, isDirectory.boolValue)
		{
		case (false, _):
			try FileManager.default.createDirectory(at: imageDir, withIntermediateDirectories: true)
		case (true, false):
			throw Simulation.Error.cannotCreateDirectory(imageDir)
		case (true, true):
			break
		}
	}

	public mutating func saveOneFrame(frame: ImageWriter.FrameData)
	{
		assert(frame.dimensions.origin == Coordinate.origin, "Cannot deal with frames whose origin is not (0, 0)")

		let size = frame.dimensions.size &* displayScale
		let image = Image(size: size)
		//let imageFileName = "frame-\(frame.generation, zeroPadding: 6)-\(frame.simStep, zeroPadding: 6).png"

		// Draw the barriers
		let barrierColour: ImageWriter.RGB = (red: 0x88, green: 0x88, blue: 0x88)
		for coordinate in frame.barrierLocations
		{
			// CImg is upside down, so the y coodinate has to be flipped
			let newCoordinate = CIMGDelegate.flipped(coordinate: coordinate, yTop: frame.dimensions.topLeft.y)
			let newOrigin = Coordinate.origin + newCoordinate.positionVector &* displayScale - displayScale / 2
			let rect = Rect(bottomLeft: newOrigin, size: displayScale)

			image.draw(rect: rect, colour: barrierColour, alpha: 1)
		}

		// Draw the individuals

		for (location, colour) in  frame.individualData
		{
			let newLocation = CIMGDelegate.flipped(coordinate: location, yTop: frame.dimensions.topLeft.y)
			image.drawCircle(centre: Coordinate.origin + newLocation.positionVector &* displayScale,
							 radius: agentSize,
							 colour: colour,
							 alpha: 1)
		}
		imageList.append(image)
	}

	public mutating func saveVideo(generation: Int)
	{
		let fileName = "gen-\(generation, zeroPadding: 6).avi"
		let fileURL = imageDir.appendingPathComponent(fileName)
		log.debug("Saving video '\(fileURL.path)'")
		imageList.saveVideo(to: fileURL, frameRate: 25, format: "H264")
		imageList.removeAll()
	}

	private static func flipped(coordinate: Coordinate, yTop: Int) -> Coordinate
	{
		let newY = yTop - coordinate.y - 1
		return Coordinate(coordinate.x, newY)
	}
}

private extension CIMGDelegate
{
	class Image
	{
		fileprivate let image: OpaquePointer

		init(size: Vector)
		{
			let sizeX = UInt32(size.x)
			let sizeY = UInt32(size.y)
			image = cimg_image(sizeX, sizeY, 1, 3, 0)
		}

		func draw(rect: Rect, colour: ImageWriter.RGB, alpha: Float)
		{
			let colours: [UInt8] = [colour.red, colour.green, colour.blue]
			cimg_drawRect(image,
						  Int32(rect.origin.x), Int32(rect.origin.y),
						  Int32(rect.topRight.x), Int32(rect.topRight.y),
						  colours,
						  alpha)
		}

		func drawCircle(centre: Coordinate, radius: Int, colour: ImageWriter.RGB, alpha: Float)
		{
			let colours: [UInt8] = [colour.red, colour.green, colour.blue]
			cimg_drawCircle(image, Int32(centre.x), Int32(centre.y), Int32(radius), colours, alpha)
		}

		deinit
		{
			cimg_delete(image);
		}
	}

	class ImageList
	{
		private let list: OpaquePointer

		init()
		{
			list = cimgList_new()
		}

		deinit
		{
			cimgList_delete(list)
		}

		func append(_ image: Image)
		{
			cimgList_append(list, image.image)
		}

		func saveVideo(to url: URL, frameRate: Int, format: String)
		{
			url.withUnsafeFileSystemRepresentation
			{
				cimgList_saveVideo(list, $0!, 25, "H264")
			}
		}

		func removeAll()
		{
			cimgList_clear(list)
		}
	}
}

private extension DefaultStringInterpolation
{
	mutating func appendInterpolation(_ anInt: Int, zeroPadding: Int)
	{
		let anIntString = String(anInt < 0 ? -anInt : anInt)
		let padCharsNeeded = zeroPadding - anIntString.count
		let padding = padCharsNeeded > 0 ? String(repeating: "0", count: padCharsNeeded) : ""
		let minus = anInt < 0 ? "-" : ""
		appendInterpolation(minus + padding + anIntString)
	}
}
