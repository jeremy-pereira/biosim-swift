//
//  Random.swift
//  
//
//  Created by Jeremy Pereira on 28/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

/// A random number generator that can be controlled by the application
///
/// Defaults to an instance of `SystemRandomNumberGenerator`
public var rng = AnyRNG.wrap(SystemRandomNumberGenerator())

public extension AnyRNG
{
	/// Returns a random yes/no based on a factor and a range
	///
	/// The factor determines the probability of returning true. If the range is
	/// `1 ... 2` and the factor is 1.8, `true` will be returned with probability
	/// `(1.8 - 1) / (2 - 1)` = 0.8
	/// - Parameters:
	///   - range: The range, defaults to `0 ... 1`
	///   - factor: The factor, must be in `range`
	/// - Returns: A random `Bool`
	func randomBool(in range: ClosedRange<Double> = 0 ... 1, factor: Double) -> Bool
	{
		assert(range.contains(factor))
		var mutableSelf = self
		return Double.random(in: range, using: &mutableSelf) < factor
	}
}
