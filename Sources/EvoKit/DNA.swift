//
//  DNA.swift
//  
//
//  Created by Jeremy Pereira on 28/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

private let log = Logger.getLogger("EvoKit.DNA")

/// Models a strand of DNA
///
/// Each "base" is a UInt8. How they are interpreted, is an organism specific
/// detail.
public typealias DNA = [UInt8]

public extension DNA
{
	/// Find the similarity to another strand of DNA
	///
	/// We should implement different ways to compare genomes but for the moment, we'll go with
	///  bitwise Hamming distance
	///  - todo: Implement other distance algorithms
	/// - Parameter other: The other genome to comapre
	/// - Returns: A number between 0 and 1 that quantifies the similarity
	func similarity(to other: DNA) -> Double
	{
		guard self.count == other.count else { return 0 }

		let maximumDistance = self.maximumDistance
		let actualDistance = self.distance(to: other)
		return Double(actualDistance) / Double(maximumDistance)
	}


	/// Make a random strand of DNA
	/// - Parameters:
	///   - minLength: Minimum length of the strand
	///   - maxLength: Maximum length of the strand must be at least as big as `minLength`
	/// - Returns: A random strands of DNA
	static func makeRandom(minLength: Int, maxLength: Int)  -> DNA
	{
		assert(minLength <= maxLength)
		let length = (minLength ... maxLength).randomElement(using: &rng)!
		var ret: [UInt8] = []
		for _ in 0 ..< length
		{
			ret.append(UInt8.random(in: 0 ... UInt8.max, using: &rng))
		}
		return ret
	}

	private static var ridWarnDone = false

	/// Do random insertions and deletions into the DNA
	///
	/// - TODO: Implement the body of this function
	/// - Returns: A strand of DNA with random insertyions and deletions
	func randomInsertionDeletion() -> DNA
	{
		if !DNA.ridWarnDone
		{
			log.warn("TODO: Implement random insertions and deletions")
			DNA.ridWarnDone = true
		}
		return self
	}

	func applyPointMutations(pointMutationRate: Double) -> DNA
	{
		var mutatingSelf = self
		for _ in 0 ..< mutatingSelf.count
		{
			if rng.randomBool(factor: pointMutationRate)
			{
				mutatingSelf.randomBitFlip()
			}
		}
		return mutatingSelf
	}

	private mutating func randomBitFlip()
	{
		let baseIndex = (startIndex ..< endIndex).randomElement(using: &rng)!
		let bitNumber = (0 ..< UInt8.bitWidth).randomElement(using: &rng)!
		self[baseIndex] ^= 1 << bitNumber
	}

	/// Recombine two DNA strands
	///
	/// PRovides a DNA strand based on segments of both parent DNA strands.
	/// - Parameters:
	///   - other: Other strrand to combine with
	///   - expectedCrossOvers: Number of crossovers we want on average
	/// - Returns: A recombined DNA strand
	func recombined(with other: DNA, expectedCrossOvers: Int) -> DNA
	{
		guard self.count >= other.count
		else { return other.recombined(with: self, expectedCrossOvers: expectedCrossOvers) }
		guard !other.isEmpty else { return self }

		assert(self.count >= other.count && !other.isEmpty)

		var newDNA = DNA()
		// Choose which gene to start with with equal chance
		var useOtherGene = rng.randomBool(factor: 0.5)
		// We want two switches on average over the length of other
		let switchChance = other.count > 1 ? (Double(expectedCrossOvers) / Double(other.count)) : 0
		for (myGene, otherGene) in zip(self, other)
		{
			newDNA.append(useOtherGene ? otherGene : myGene)
			if EvoKit.rng.randomBool(factor: switchChance)
			{
				useOtherGene = !useOtherGene
			}
		}
		if self.count > other.count
		{
			newDNA.append(contentsOf: self[other.count ..< self.count])
		}
		return newDNA
	}

}

