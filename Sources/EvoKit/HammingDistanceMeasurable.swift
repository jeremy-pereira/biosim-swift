//
//  HammingDistanceMeasurable.swift
//  
//
//  Created by Jeremy Pereira on 28/12/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


/// Protocol for an algorithm to measure distance between two objects of the
/// same type.
///
/// Any `FixedWidthInteger` can be made conformant simply by declaring
/// conformance. Any collection of ``HammingDistanceMeasurable`` objects can be
/// made conformant by simply declaring conformnce.
///
/// Mrthods from this protocol are used tyopically to measure the similarity
/// between DNA strands. This could also be used as a scoring criterion for
/// some genetic algorithms.
public protocol HammingDistanceMeasurable
{

	/// Calculate the distance between two objects
	///  - Parameters:
	///      - rhs: another object to measure the distance to
	/// - Returns: The distance as an integer between the two objects
	func distance(to rhs: Self) -> Int
	/// The maximum possible distance between this and another object that is
	/// the same size or smaller than this object
	var maximumDistance: Int { get }
	/// Are objects of this type fixed width
	///
	/// Allows the collection extension to optimise calculations for fixed width elements
	static var isFixedWidth: Bool { get }
}

public extension HammingDistanceMeasurable where Self: FixedWidthInteger
{
	func distance(to rhs: Self) -> Int
	{
		return (self ^ rhs).nonzeroBitCount
	}

	var maximumDistance: Int { self.bitWidth }

	static var isFixedWidth: Bool { true }
}

extension Array: HammingDistanceMeasurable
where Element: HammingDistanceMeasurable
{
	public func distance(to rhs: Self) -> Int
	{
		let comparison = zip(self, rhs).reduce(0)
		{
			partialResult, measurables in
			return partialResult + measurables.0.distance(to: measurables.1)
		}
		// If the sequence lengths are unequal, add on the max possible distance
		// for the left over
		let extraDistance: Int
		let lengthDifference = self.count - rhs.count
		if lengthDifference != 0
		{
			if Self.Element.isFixedWidth
			{
				extraDistance = lengthDifference > 0 ? (self.first!.maximumDistance * lengthDifference)
													 : (rhs.first!.maximumDistance * -lengthDifference)
			}
			else if lengthDifference > 0
			{
				let startIndex = self.index(self.startIndex, offsetBy: self.count - lengthDifference)
				let excessSequence = self[startIndex ..< self.endIndex]
				extraDistance = excessSequence.reduce(0)
				{
					partialResult, measurable in
					partialResult + measurable.maximumDistance
				}
			}
			else
			{
				let startIndex = rhs.index(rhs.startIndex, offsetBy: rhs.count + lengthDifference)
				let excessSequence = rhs[startIndex ..< rhs.endIndex]
				extraDistance = excessSequence.reduce(0)
				{
					partialResult, measurable in
					partialResult + measurable.maximumDistance
				}
			}
		}
		else
		{
			extraDistance = 0
		}
		return comparison + extraDistance
	}

	public var maximumDistance: Int
	{
		if Self.Element.isFixedWidth
		{
			return self.count * (self.first?.maximumDistance ?? 0)
		}
		else
		{
			return self.reduce(0) { partialResult, measurable in partialResult + measurable.maximumDistance }
		}
	}

	public static var isFixedWidth: Bool { false }
}
extension UInt8: HammingDistanceMeasurable {}
extension Int16: HammingDistanceMeasurable {}
extension UInt16: HammingDistanceMeasurable {}
