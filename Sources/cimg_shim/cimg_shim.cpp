//
//  cimg_shim.cpp
//  
//
//  Created by Jeremy Pereira on 02/01/2022.
//

#include "cimg_shim.h"
#define cimg_display 0
#include "CImg.h"

// MARK: -
// MARK: Cimg

struct CimgHandle
{
	CimgHandle(unsigned int size_x,
			   unsigned int size_y,
			   unsigned int size_z,
			   unsigned int colourChannels,
			   uint8_t initialValue)
	{
		image = cimg_library::CImg<uint8_t>(size_x, size_y, size_z, colourChannels, initialValue);
	}

	cimg_library::CImg<uint8_t> image;
};

struct CimgHandle* cimg_image(unsigned int size_x,
						      unsigned int size_y,
						      unsigned int size_z,
						      unsigned int colourChannels,
						      uint8_t initialValue)
{
	struct CimgHandle* ret = new CimgHandle(size_x, size_y, size_z, colourChannels, initialValue);
	return ret;
}

void
cimg_drawRect(struct CimgHandle* _Nonnull cimg, int x0, int y0, int x1, int y1, const uint8_t*  _Nonnull const colour, float opacity)
{
	cimg->image.draw_rectangle(x0, y0, x1, y1, colour, opacity);
}

void
cimg_drawCircle(struct CimgHandle* _Nonnull cimg, int x, int y, int radius, const uint8_t*  _Nonnull const colour, float opacity)
{
	cimg->image.draw_circle(x, y, radius, colour, opacity);
}

void cimg_delete(struct CimgHandle* _Nonnull image)
{
	delete image;
}

// MARK: -
// MARK: CimgList


struct CimgListHandle
{
	cimg_library::CImgList<uint8_t> imageList;
};

struct CimgListHandle* _Nonnull
cimgList_new()
{
	return new CimgListHandle;
}

void
cimgList_append(struct CimgListHandle* _Nonnull list, struct CimgHandle* _Nonnull image)
{
	list->imageList.push_back(image->image);
}

void
cimgList_saveVideo(struct CimgListHandle* _Nonnull list,
				   const char* _Nonnull const fileName,
				   int frameRate,
				   const char* _Nonnull const format)
{
	list->imageList.save_video(fileName);
}

void
cimgList_clear(struct CimgListHandle* _Nonnull list)
{
	list->imageList.clear();
}


void
cimgList_delete(struct CimgListHandle* _Nonnull list)
{
	delete list;
}
