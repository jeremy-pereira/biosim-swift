//
//  cimg_shim.hpp
//  
//
//  Created by Jeremy Pereira on 02/01/2022.
//
//  Copyright (c) Jeremy Pereira 2022
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#ifndef cimg_shim_h
#define cimg_shim_h

#include <stdint.h>

#if defined(__cplusplus)
extern "C" {
#endif

// MARK: -
// MARK: Cimg

/// An opaque handle for a CImg
struct CimgHandle;

/// Construct a CImg image
///
/// The coordinate sytem has the origin at the top left corner of the image.
///
/// @param size_x The x dimension in pixels
/// @param size_y The y dimension in pixels
/// @param size_z The z dimension in pixels
/// @param size_c The number of colour channels (1 ... 4)
/// @param initialValue Initial value for each pixel
/// @return A handle to a Cimg image
struct CimgHandle* _Nonnull
cimg_image(unsigned int size_x,
		   unsigned int size_y,
		   unsigned int size_z,
		   unsigned int size_colourChannels,
		   uint8_t initialValue);

/// Draw a rectangle
/// @param cimg handle to the CImg object
/// @param x0 X coordinate of top left corner
/// @param y0 y coordinate of the top left coordinate
/// @param x1 x coordinate of bottom right corner
/// @param y1 y coordinate of bottom right corner
/// @param colour A three byte array of RGB values
/// @param opacity The opacity - a number between zero and 1
void
cimg_drawRect(struct CimgHandle* _Nonnull cimg, int x0, int y0, int x1, int y1, const uint8_t* const _Nonnull colour, float opacity);

/// Draw a circle in an image
/// @param cimg Image to draw into
/// @param x x coordinate of centre
/// @param y y coordinate of centre
/// @param radius radius of circle
/// @param colour colour of circle
/// @param opacity Opacity of colour
void
cimg_drawCircle(struct CimgHandle* _Nonnull cimg, int x, int y, int radius, const uint8_t*  _Nonnull const colour, float opacity);


/// Delete an image we don't need anymore
/// @param image The image to delete
void cimg_delete(struct CimgHandle* _Nonnull image);

// MARK: -
// MARK: CimgList

/// An opaque handle for a CImgList
struct CimgListHandle;


/// Create a new empty cimg list
///
/// @return A new empty list
struct CimgListHandle* _Nonnull
cimgList_new();

/// Append an image to an image list
/// @param list The list to which to append the image
/// @param image The image
void
cimgList_append(struct CimgListHandle* _Nonnull list, struct CimgHandle* _Nonnull image);


/// Save the frames in this list as a video
/// @param list list containing image frames
/// @param fileName  where to save the file
/// @param frameRate Frame rate
/// @param format Video format
void
cimgList_saveVideo(struct CimgListHandle* _Nonnull list,
				   const char* _Nonnull const fileName,
				   int frameRate,
				   const char* _Nonnull const format);

/// Clear all the images out of the list
/// @param list The list to clear
void cimgList_clear(struct CimgListHandle* _Nonnull list);
/// Delete an image list we don't need anymore
/// @param image The image list to delete
void
cimgList_delete(struct CimgListHandle* _Nonnull list);

#if defined(__cplusplus)
}
#endif

#endif /* cimg_shim_hpp */
