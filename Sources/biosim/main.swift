//
//  main.swift
//  
//
//  Created by Jeremy Pereira on 28/11/2021.
//
//  Copyright (c) Jeremy Pereira 2021
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import ArgumentParser
import biosim_swift
import Toolbox

struct BioSim: ParsableCommand
{
	@Argument(help: "Configuration file for the simulation, defaults to biosim.ini")
	var config: String = "biosim.ini"

	func run() throws
	{
		//let parameters = try ParamManager(fileName: config)
		let params: [String : ParamManager.ParamType] = [
			"maxGenerations" : 1000,
			"videoStride" : 50,
			"population" : 300,
		]
		let parameters = try ParamManager(parameters: params)
		let simulator = try Simulation(parameters: parameters)
		Logger.set(level: .debug, forName: "biosim-swift.ImageWriter")
		Logger.set(level: .debug, forName: "biosim-swift.CIMGDelegate")
		try simulator.run()
	}
}

Logger.set(level: .info, forName: "biosim-swift")

BioSim.main()
